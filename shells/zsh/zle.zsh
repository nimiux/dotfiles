#bindkey -M viins 'jk' vi-cmd-mode
#bindkey -M viins 'rt' vi-cmd-mode

bindkey '\eu' undo
bindkey -s '\es' 'hello world!'

function _git-status {
    zle kill-whole-line
    zle -U "git status"
    zle accept-line
}
zle -N _git-status
bindkey '\eg' _git-status
