#!/bin/sh
#
# $Header: /etc/acpi/default.sh                          Exp $
# $Author: (c) 2012-2014 -tclover <tokiclover@dotfiles.> Exp $
# $License: MIT (or 2-clause/new/simplified BSD)         Exp $
# $Version: 2014/12/24 21:09:26                          Exp $
#

AUDIOUSER="me"
log() { logger -p daemon.info "ACPI: $*"; }
uhd() { log "event unhandled: $*"; }
suspend2ram() {
    log "Suspending to RAM..."
    #s2ram -f -p -m -s
    echo mem > /sys/power/state
}
suspend2disk() {
    [[ "${1}" == "00000000" ]] && log "Suspending to Disk" \
        && echo disk > /sys/power/state
}
togglewifi() {
    if [[ $(nmcli radio wifi) == "disabled" ]] ; then
        log "Enabling wifi"
        nmcli radio wifi on
    else
        log "Disabling wifi"
        nmcli radio wifi off
    fi
}

set ${*}
group=${1%/*}
action=${1#*/}
device=${2}
id=${3}
value=${4}

[ -d /dev/snd ] && alsa=true || alsa=false
[ -d /dev/oss ] && oss=true || oss=false
amixer="amixer -q set Master"
ossmix="ossmix -- vmix0-outvol"

log "MSG: ${group} ${action} ${device} ${id} ${value} ALSA: ${alsa}"


case ${group} in
    ac_adapter)
        case ${value} in
            *0)
                       log "TBI: switching to power.bat power profile"
                hprofile power.bat
            ;;
            *1)
                log "TBI: switching to power.adp power profile"
                hprofile power.adp
            ;;
            *)
                uhd ${*}
            ;;
        esac
        ;;
    battery)
        case ${value} in
            *0)
                       log "TBI: switching to power.adp power profile"
                hprofile power.adp
            ;;
            *1)
                log "TBI: switching to power.adp power profile"
                hprofile power.adp
            ;;
            *)
                uhd ${*}
            ;;
        esac
        ;;
    button)
        case ${action} in
            #lid)
            #    case ${id} in
            #        close)
            #            suspend2ram
            #        ;;
            #        open)
            #                   :
            #        ;;
            #        *)
            #                   uhd ${*}
            #        ;;
            #    esac
            #;;
            #power)
            #    suspend2disk ${value}
            #;;
            sleep)
                suspend2ram
            ;;
            mute)
                ${alsa} && ${amixer} toggle
            ;;
            volumeup)
                ${alsa} && ${amixer} 3dB+
                ${oss} && ${ossmix} +3
            ;;
            volumedown)
                ${alsa} && ${amixer} 3dB-
                ${oss} && ${ossmix} -3
            ;;
            wlan)
                togglewifi
            ;;
            *)
                uhd ${*}
            ;;
        esac
        ;;
    cd)
        case ${action} in
            play)
                sudo -u ${AUDIOUSER} xmms2 toggle
            ;;
            stop)
                sudo -u ${AUDIOUSER} xmms2 stop
            ;;
            prev)
                sudo -u ${AUDIOUSER} xmms2 prev
            ;;
            next)
                sudo -u ${AUDIOUSER} xmms2 next
            ;;
            *)
                uhd $*
            ;;
        esac
        ;;
    jack)
        case ${id} in
            *plug)
                :
            ;;
            *)
                uhd ${*}
            ;;
        esac
        ;;
    video)
        case ${action} in
            displayoff)
                :
            ;;
            brightnessup)
                /etc/acpi/intel-keyboard-backlight.sh up
            ;;
            brightnessdown)
                /etc/acpi/intel-keyboard-backlight.sh down
            ;;
            *)
                uhd ${*}
            ;;
        esac
        ;;
    *)
               uhd ${*}
    ;;
esac

set alsa oss amixer ossmix group action device id

# vim:fenc=utf-8:ft=sh:ci:pi:sts=4:sw=4:ts=4:
