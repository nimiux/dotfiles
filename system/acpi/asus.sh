#!/bin/sh

MYUSER="chema"
LOCKPROGRAM="/usr/bin/slock"
log() { logger -p daemon.info "ACPI: $*"; }
uhd() { log "event unhandled: $*"; }
suspend2ram() {
    log "Suspending to RAM..."
    #s2ram -f -p -m -s
    echo mem > /sys/power/state
}
suspend2disk() {
    [[ "${1}" == "00000000" ]] && log "Suspending to Disk" \
        && echo disk > /sys/power/state
}
lockscreen() {
    #DISPLAY=:0 su -c - ${MYUSER} ${LOCKPROGRAM}
    pgrep $LOCKPROGRAM > /dev/null 2>&1
    if [[ $? -ne 0 ]] ; then
        DISPLAY=:0 su -c - ${MYUSER} ${LOCKPROGRAM}
    fi
}

set ${*}
group=${1%/*}
action=${1#*/}
device=${2}
id=${3}
value=${4}

[ -d /dev/snd ] && alsa=true || alsa=false
[ -d /dev/oss ] && oss=true || oss=false
amixer="amixer sset Master"
ossmix="ossmix -- vmix0-outvol"

log "MSG: ${group} ${action} ${device} ${id} ${value} ALSA: ${alsa}"

case ${group} in
    #0B3CBB35-E3C2-)
    #    case ${action} in
    #        0B3CBB35-E3C2-)
    #            touchpadservice="asus_touchpad_numpad.service"
    #            if [ "$(systemctl is-active $touchpadservice)" = "active" ] ; then                                          ✔
    #                systemctl stop $touchpadservice
    #            else
    #                systemctl start $touchpadservice
    #            fi
    #        ;;
    #    esac
    #;;
    #ac_adapter)
    #    case ${value} in
    #        *0)
    #            log "TBI: switching to power.bat power profile"
    #            hprofile power.bat
    #        ;;
    #        *1)
    #            log "TBI: switching to power.adp power profile"
    #            hprofile power.adp
    #        ;;
    #        *)
    #            uhd ${*}
    #        ;;
    #    esac
    #    ;;
    #battery)
    #    case ${value} in
    #        *0)
    #            log "TBI: switching to power.adp power profile"
    #            hprofile power.adp
    #        ;;
    #        *1)
    #            log "TBI: switching to power.adp power profile"
    #            hprofile power.adp
    #        ;;
    #        *)
    #            uhd ${*}
    #        ;;
    #    esac
    #    ;;
    button)
        case ${action} in
            lid)
                case ${id} in
                    close)
                    lockscreen
                    ;;
                    #open)
                    #           :
                    #;;
                    *)
                               uhd ${*}
                    ;;
                esac
            ;;
            power)
        if [[ "$value" == "00000000" ]] ; then
            # poweroff is "00000000". poweron is "00000006"
            lockscreen
        fi
            ;;
            #sleep)
            #    suspend2ram
            #;;
            mute)
                #${alsa} && ${amixer} toggle
        ##amixer -D pulse set Master 1+ toggle
        ##pactl set-sink-mute 0 toggle
        #sudo -u chema amixer sset Master toggle
            ;;
            volumeup)
                #${alsa} && ${amixer} 1dB+
                #${oss} && ${ossmix} +1
            ;;
            volumedown)
                #${alsa} && ${amixer} 1dB-
                #${oss} && ${ossmix} -1
            ;;
            #wlan)
            #    togglewifi
            #;;
            #*)
            #    uhd ${*}
            #;;
        esac
        ;;
    #cd)
    #    case ${action} in
    #        play)
    #            sudo -u ${MYUSER} xmms2 toggle
    #        ;;
    #        stop)
    #            sudo -u ${MYUSER} xmms2 stop
    #        ;;
    #        prev)
    #            sudo -u ${MYUSER} xmms2 prev
    #        ;;
    #        next)
    #            sudo -u ${MYUSER} xmms2 next
    #        ;;
    #        *)
    #            uhd $*
    #        ;;
    #    esac
    #    ;;
    jack)
        case ${id} in
            *plug)
                uhd ${*}
            ;;
            *)
                uhd ${*}
            ;;
        esac
        ;;
    video)
        case ${action} in
            displayoff)
                uhd ${*}
            ;;
            brightnessup)
                xbacklight -inc 2
            ;;
            brightnessdown)
                xbacklight -dec 2
            ;;
            *)
                uhd ${*}
            ;;
        esac
        ;;
    *)
               uhd ${*}
    ;;
esac

set alsa oss amixer ossmix group action device id

# vim:fenc=utf-8:ft=sh:ci:pi:sts=4:sw=4:ts=4:
