#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use feature ':5.16';

(>>>POINT<<<)

__END__

=encoding utf8

=head1 NAME

(>>>FILE<<<) - (>>>DESC<<<)

=head1 SYNOPSIS

(>>>FILE<<<) [options] args

    -opt --long      Option description
    -v   --verbose   show debugging information
    -h   --help      show help

=head1 DESCRIPTION

Stub documentation for (>>>FILE<<<), 

Created: (>>>ISO-DATE<<<)

=head1 TODO

Things to be done.

=head1 SEE ALSO

See more information at....

=head1 VERSION

This man page documents (>>>FILE<<<) version (>>>VERSION<<<) 

=head1 AUTHOR

(>>>USER_NAME<<<) E<lt>nimiux@freeshell.deE<gt>

=head1 COPYRIGHT

Copyright (c) (>>>YEAR<<<)-(>>>YEAR<<<) by (>>>USER_NAME<<<). All rights reserved

=head1 LICENSE

This package is free software; you can use, modify and redistribute
it under the same terms as Perl itself, i.e., at your option, under
the terms either of the "Artistic License 2.0".

=head1 BUGS

Report bugs at:

https://...

=cut
