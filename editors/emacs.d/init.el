;; Workarounds
; see https://github.com/kiwanami/emacs-epc/issues/35
(setq byte-compile-warnings '(cl-functions))

;;toggle wrapping text at this column
(setq-default fill-column 80)

;; You will most likely need to adjust this font size for your system!
(defvar chema/default-font-size 150)
(defvar chema/default-variable-font-size 150)
(defvar chema/default-font (format "Fantasque Sans Mono:style=Regular:size=%d" chema/default-font-size))
;; Make frame transparency overridable
(defvar chema/frame-transparency '(99 . 99))
; Diary
(setq diary-file "~/diary")
; TAGS
(setq tags-file-name "~/TAGS")
; Files
(setq large-file-warning-threshold 1000000000)
;; Spaces in sentences
(setq sentence-end-double-space nil)
;; Help
(setq apropos-sort-by-scores t)
;;(setq backup-by-copying t)                      ; don't clobber symlinks
;;(setq delete-old-versions t)
;;(setq version-control t)                        ; make backup versions unconditionally
;;(setq delete-old-versions -1)                   ; delete excess backups silently
;;(setq vc-make-backup-files t)                   ; make backups of of registered files
;;(setq vc-follow-symlinks 'ask)                  ; ask when visiting symlinks

;;; Autorevert
;;(global-auto-revert-mode 1)
;;(setq global-auto-revert-non-file-buffers t)

;;(setq ring-bell-function 'ignore)               ; silent bell on mistakes

;;(setq delete-selection-mode 1)                  ;

;;(setq read-file-name-completion-ignore-case t)  ;
;;;(global-display-line-numbers-mode)            ; display line numbers in all buffers
;;
;;;; Version control
;;

;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))
(defun chema/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                     (time-subtract after-init-time before-init-time)))
           gcs-done))
(add-hook 'after-init-hook (lambda() (ido-mode -1)))
;;(add-hook 'emacs-startup-hook #'chema/display-startup-time)

(setq coding-system-for-read 'utf-8)            ;
(setq coding-system-for-write 'utf-8)           ;

;; Initialize package sources
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

(use-package auto-package-update
  :custom
  (auto-package-update-interval 7)
  (auto-package-update-prompt-before-update t)
  (auto-package-update-hide-results t)
  :config
  (auto-package-update-maybe)
  (auto-package-update-at-time "09:00"))

;; NOTE: If you want to move everything out of the ~/.emacs.d folder
;; reliably, set `user-emacs-directory` before loading no-littering!
;(setq user-emacs-directory "~/.cache/emacs")
(use-package no-littering)

;; no-littering doesn't set this by default so we must place
;; auto save files in the same path as it uses for sessions
;; Customization goes into a separate file
(setq auto-save-file-name-transforms
    `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
(setq custom-file (locate-user-emacs-file (concat (file-name-as-directory no-littering-etc-directory) "custom-vars.el")))
(load custom-file 'noerror 'nomessage)

(setq inhibit-startup-message t)
(setq-default indent-tabs-mode nil)
(setq confirm-kill-emacs nil)                   ; don't ask when leaving
(setq inhibit-startup-screen t)                 ; don't show the startup screen
;;(setq initial-scratch-message "Emacs ready.") ; initial message
(setq history-length 25)                        ; history to be saved
(setq use-dialog-box nil)                       ; don't use dialog boxes
(setq visible-bell t)                           ; set up the visible bell
(scroll-bar-mode -1)                            ; disable visible scrollbar
(tool-bar-mode -1)                              ; disable the toolbar
(tooltip-mode -1)                               ; disable tooltips
(set-fringe-mode 10)                            ; give some breathing room
(menu-bar-mode -1)                              ; no need for the menu bars
(fset 'menu-bar-open nil)                       ; disable menu
;(toggle-scroll-bar nil)                         ; no vertical scroll bar
;;(display-time-mode)                             ; display time and more info in the modeline
;;(display-battery-mode)                          ; display the battery status in the modeline
(save-place-mode)                               ; remember the place of point in file
(set-fringe-mode 10)                            ; give some breathing room
(tooltip-mode nil)                              ; disable tooltips
(column-number-mode)
;;(company-quickhelp-mode)
(recentf-mode)
;; Disable line numbers for some modes
;;(dolist (mode '(org-mode-hook
;;                term-mode-hook
;;                shell-mode-hook
;;                treemacs-mode-hook
;;                eshell-mode-hook))
;;  (add-hook mode (lambda () (display-line-numbers-mode 0))))
;;(global-display-line-numbers-mode t)
;; Set frame transparency
(set-frame-parameter (selected-frame) 'alpha chema/frame-transparency)
(add-to-list 'default-frame-alist `(alpha . ,chema/frame-transparency))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))
;;;; Frame
;;(set-frame-parameter nil 'fullscreen 'fullboth)
;;;(se(setq inhibit-splash-screen t)

(defvar chema/my-theme 'doom-molokai)
;; (setq default-frame-alist '(
;;                            ;(alpha . (92 . 92))
;;                            ;(background-color . "#101416")
;;                            ;(foreground-color . "#f3dde6")
;;                           `(font . ,chema/default-font)))
(set-face-attribute 'default nil :font "Fantasque Sans Mono" :height chema/default-font-size)
;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "Fantasque Sans Mono" :height chema/default-font-size)
;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Fantasque Sans Mono" :height chema/default-variable-font-size :weight 'regular)
;; This is for server
(defun load-doom-theme (frame)
  (select-frame frame)
  (set-face-attribute 'default nil :font "Fantasque Sans Mono" :height chema/default-font-size)
  (load-theme chema/my-theme t))
(if (daemonp)
  (add-hook 'after-make-frame-functions #'load-doom-theme)
  (load-theme chema/my-theme t))

(use-package general
    :after evil
    :config
    (general-create-definer chema/leader-keys
      :keymaps '(normal insert visual emacs)
      :prefix "SPC"
      :global-prefix "C-SPC")
    (chema/leader-keys
      "RET" '(lambda () (interactive) (eshell))
      "DEL" '(lambda () (interactive) (run-perl))
      "/"  '(counsel-ag :which-key "search through files")
      "("  '(paredit-mode :which-key "paredit mode")
      ":"  '(counsel-M-x :which-key "run command")
      ";"  '(comment-line :which-key "insert comment")
      "1"  '(delete-other-windows :which-key "leave only current window")
      "2"  '(split-window-right :which-key "split vertically")
      "a"  '(evil-append-line :which-key "append to end of line")
      "b"  '(ivy-switch-buffer :which-key "switch buffer")
      "cfg" '(lambda () (interactive) (find-file (expand-file-name "~/.emacs.d/emacs.org")))
      "d"  '(dired :which-key "dired")
      "k" '(kill-this-buffer :which-key "kill current buffer")
      "m" '(mu4e :which-key "mu4e")
      "n" '(next-buffer :which-key "next buffer")
      "ñ" '(other-window :which-key "other-window")
      "o"  '(counsel-find-file :which-key "find file")
      "p" '(previous-buffer :which-key "previous buffer")
      "q" '(save-buffers-kill-terminal :which-key "save and exit")
      "r" '(crux-recentf-find-file :which-key "recent file")
      "s"  '(:ignore t :which-key "switch or swap")
      "sb"  '(ivy-switch-buffer :which-key "switch buffer")
      "ss" '(swiper :which-key "search in document")
      "sw" '(window-swap-states :which-key "transpose windows")
      "t"  '(:ignore t :which-key "toggles")
      "ts" '(hydra-text-scale/body :which-key "scale text")
      "tt" '(counsel-load-theme :which-key "choose theme")
      "tw" '(toggle-window-split :which-key "toggle windows")
      "w" '(save-buffer :which-key "save buffer")))

  (use-package evil
    :init
    (setq evil-want-integration t)
    (setq evil-want-keybinding nil)
    (setq evil-want-C-u-scroll t)
    (setq evil-want-C-i-jump nil)
    :config
    (evil-mode 1)
    (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
    (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)
    (define-key evil-normal-state-map (kbd "SPC SPC") 'evil-visual-block)
    ;;Exit insert mode by pressing j and then j quickly
    (require 'key-chord)
    (setq key-chord-two-keys-delay 0.5)
    (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
    (key-chord-mode 1)
    ;; Use visual line motions even outside of visual-line-mode buffers
    (evil-global-set-key 'motion "j" 'evil-next-visual-line)
    (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
    (evil-set-initial-state 'messages-buffer-mode 'normal)
    (evil-set-initial-state 'dashboard-mode 'normal))

  (use-package undo-fu
    :custom
    (evil-undo-system 'undo-fu))

  (use-package undo-fu-session
    :config
    (setq undo-fu-session-incompatible-files '("/COMMIT_EDITMSG\\'" "/git-rebase-todo\\'")))
  (global-undo-fu-session-mode)
  ;;(use-package undo-fu-session)
  ;;(autoload 'ispell-get-word "ispell")

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package evil-numbers
  :config
  (global-set-key (kbd "s-+") 'evil-numbers/inc-at-pt)
  (global-set-key (kbd "s--") 'evil-numbers/dec-at-pt))

(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))

(use-package expand-region)
(use-package find-file-in-project)
(use-package keychain-environment
  :init
  (keychain-refresh-environment))
(use-package crux)                ; a collection of ridicously used extensions
(use-package flycheck
  :init
  (global-flycheck-mode))
(use-package lorem-ipsum)         ; insert dummy pseudo Latin text
(use-package multiple-cursors)    ; handle multiple cursors
(use-package nlinum-relative      ; line numbers
  :config
  ;(nlinum-relative-setup-evil)
  (add-hook 'prog-mode-hook 'nlinum-relative-mode))
(use-package openwith
  :config
  (openwith-mode t)
  (setq openwith-associations '(("\\.pdf\\'" "evince"  (file))
                                ("\\.\\(?:mp3\\|ogg\\)\\'" "mplayer"  (file))
                                ("\\.\\(?:mpe?g\\|avi\\|mp4\\|webm\\|mkv\\)\\'" "mplayer"  ("-idx" file))
                                ("\\.\\(?:jp?g\\|png\\)\\'" "display"  (file)))))

(use-package powerthesaurus)

(use-package whole-line-or-region
  :config
  (add-hook 'after-init-hook (whole-line-or-region-global-mode)))

(use-package counsel-etags
  :ensure t
  :bind (("C-]" . counsel-etags-find-tag-at-point))
  :init
  (add-hook 'prog-mode-hook
        (lambda ()
          (add-hook 'after-save-hook
            'counsel-etags-virtual-update-tags 'append 'local)))
  :config
  (setq counsel-etags-update-interval 60)
  (push "build" counsel-etags-ignore-directories))

(use-package web-mode)
;;(use-package ag)

;;(require 'bl-fns)
;;(require 'init-ace-window)
;;(require 'init-doom-modeline)
;;(require 'init-mail)
;;(require 'init-org-caldav)
;;(require 'init-org-mode)
;;(require 'init-calfw-org)

;; Maxima mode
(add-to-list 'load-path "/usr/share/emacs/site-lisp/maxima/")
(autoload 'maxima-mode "maxima" "Maxima mode" t)
(autoload 'imaxima "imaxima" "Frontend for maxima with Image support" t)
(autoload 'maxima "maxima" "Maxima interaction" t)
(autoload 'imath-mode "imath" "Imath mode for math formula input" t)
(setq imaxima-use-maxima-mode-flag t)
(add-to-list 'auto-mode-alist '("\\.ma[cx]\\'" . maxima-mode))

(use-package command-log-mode
  :commands command-log-mode)

(use-package doom-themes
  :init (load-theme 'doom-molokai t))

(use-package all-the-icons)
; run all-the-icons-install-fonts

(use-package doom-modeline
  :init
  (doom-modeline-mode 1)
  :custom
  ((doom-modeline-height 15)))

(use-package which-key
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 1))

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package ivy-rich
  :after ivy
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :bind (("C-M-j" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (counsel-mode 1))

(use-package ivy-prescient
  :after counsel
  :custom
  (ivy-prescient-enable-filtering nil)
  :config
  ;; Uncomment the following line to have sorting remembered across sessions!
  ;(prescient-persist-mode 1)
  (ivy-prescient-mode 1))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package hydra
  :defer t)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(defun chema/org-font-setup ()
    ;; Replace list hyphen with dot
    (font-lock-add-keywords 'org-mode
                            '(("^ *\\([-]\\) "
                            (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

;; Set faces for heading levels
(dolist (face '((org-level-1 . 1.2)
                (org-level-2 . 1.1)
                (org-level-3 . 1.05)
                (org-level-4 . 1.0)
                (org-level-5 . 1.1)
                (org-level-6 . 1.1)
                (org-level-7 . 1.1)
                (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Fira Code" :weight 'regular :height (cdr face)))

    ;; Ensure that anything that should be fixed-pitch in Org files appears that way
    (set-face-attribute 'org-block nil    :foreground nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
    (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
    (set-face-attribute 'org-code nil     :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
    (set-face-attribute 'org-checkbox nil  :inherit 'fixed-pitch)
    (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
    (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch))

(defun chema/org-mode-setup ()
    (org-indent-mode)
    (variable-pitch-mode 1)
    (visual-line-mode 1))

(use-package org
    :pin org
    :commands (org-capture org-agenda)
    :hook (org-mode . chema/org-mode-setup)
    :config
    (setq org-ellipsis " ▾")

    (setq org-agenda-start-with-log-mode t)
    (setq org-log-done 'time)
    (setq org-log-into-drawer t)

    (setq org-agenda-files
        '("~/Projects/Code/emacs-from-scratch/OrgFiles/Tasks.org"
            "~/Projects/Code/emacs-from-scratch/OrgFiles/Habits.org"
            "~/Projects/Code/emacs-from-scratch/OrgFiles/Birthdays.org"))

    (require 'org-habit)
    (add-to-list 'org-modules 'org-habit)
    (setq org-habit-graph-column 60)

    (setq org-todo-keywords
    '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
        (sequence "BACKLOG(b)" "PLAN(p)" "READY(r)" "ACTIVE(a)" "REVIEW(v)" "WAIT(w@/!)" "HOLD(h)" "|" "COMPLETED(c)" "CANC(k@)")))

    (setq org-refile-targets
    '(("Archive.org" :maxlevel . 1)
        ("Tasks.org" :maxlevel . 1)))

    ;; Save Org buffers after refiling!
    (advice-add 'org-refile :after 'org-save-all-org-buffers)

    (setq org-tag-alist
    '((:startgroup)
        ; Put mutually exclusive tags here
        (:endgroup)
        ("@errand" . ?E)
        ("@home" . ?H)
        ("@work" . ?W)
        ("agenda" . ?a)
        ("planning" . ?p)
        ("publish" . ?P)
        ("batch" . ?b)
        ("note" . ?n)
        ("idea" . ?i)))

    ;; Configure custom agenda views
    (setq org-agenda-custom-commands
    '(("d" "Dashboard"
    ((agenda "" ((org-deadline-warning-days 7)))
        (todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))
        (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))

    ("n" "Next Tasks"
    ((todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))))

    ("W" "Work Tasks" tags-todo "+work-email")

    ;; Low-effort next actions
    ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
    ((org-agenda-overriding-header "Low Effort Tasks")
        (org-agenda-max-todos 20)
        (org-agenda-files org-agenda-files)))

    ("w" "Workflow Status"
    ((todo "WAIT"
            ((org-agenda-overriding-header "Waiting on External")
            (org-agenda-files org-agenda-files)))
        (todo "REVIEW"
            ((org-agenda-overriding-header "In Review")
            (org-agenda-files org-agenda-files)))
        (todo "PLAN"
            ((org-agenda-overriding-header "In Planning")
            (org-agenda-todo-list-sublevels nil)
            (org-agenda-files org-agenda-files)))
        (todo "BACKLOG"
            ((org-agenda-overriding-header "Project Backlog")
            (org-agenda-todo-list-sublevels nil)
            (org-agenda-files org-agenda-files)))
        (todo "READY"
            ((org-agenda-overriding-header "Ready for Work")
            (org-agenda-files org-agenda-files)))
        (todo "ACTIVE"
            ((org-agenda-overriding-header "Active Projects")
            (org-agenda-files org-agenda-files)))
        (todo "COMPLETED"
            ((org-agenda-overriding-header "Completed Projects")
            (org-agenda-files org-agenda-files)))
        (todo "CANC"
            ((org-agenda-overriding-header "Cancelled Projects")
            (org-agenda-files org-agenda-files)))))))

    (setq org-capture-templates
    `(("t" "Tasks / Projects")
        ("tt" "Task" entry (file+olp "~/Projects/Code/emacs-from-scratch/OrgFiles/Tasks.org" "Inbox")
            "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)

        ("j" "Journal Entries")
        ("jj" "Journal" entry
            (file+olp+datetree "~/Projects/Code/emacs-from-scratch/OrgFiles/Journal.org")
            "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
            ;; ,(dw/read-file-as-string "~/Notes/Templates/Daily.org")
            :clock-in :clock-resume
            :empty-lines 1)
        ("jm" "Meeting" entry
            (file+olp+datetree "~/Projects/Code/emacs-from-scratch/OrgFiles/Journal.org")
            "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
            :clock-in :clock-resume
            :empty-lines 1)

        ("w" "Workflows")
        ("we" "Checking Email" entry (file+olp+datetree "~/Projects/Code/emacs-from-scratch/OrgFiles/Journal.org")
            "* Checking Email :email:\n\n%?" :clock-in :clock-resume :empty-lines 1)

        ("m" "Metrics Capture")
        ("mw" "Weight" table-line (file+headline "~/Projects/Code/emacs-from-scratch/OrgFiles/Metrics.org" "Weight")
        "| %U | %^{Weight} | %^{Notes} |" :kill-buffer t)))

    (define-key global-map (kbd "C-c j")
    (lambda () (interactive) (org-capture nil "jj")))

    (chema/org-font-setup))

(use-package org-bullets
    :hook (org-mode . org-bullets-mode)
    :custom
    (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun chema/org-mode-visual-fill ()
    (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
    (visual-fill-column-mode 1))

(use-package visual-fill-column
    :hook (org-mode . chema/org-mode-visual-fill))

(with-eval-after-load 'org
    (org-babel-do-load-languages
        'org-babel-load-languages
        '((emacs-lisp . t)
        (python . t)))

    (push '("conf-unix" . conf-unix) org-src-lang-modes))

(with-eval-after-load 'org
    ;; This is needed as of Org 9.2
    (require 'org-tempo)
    (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
    (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
    (add-to-list 'org-structure-template-alist '("py" . "src python")))

;; Automatically tangle our Emacs.org config file when we save it
(defun chema/org-babel-tangle-config ()
    (when (string-equal (file-name-directory (buffer-file-name))
                        (file-truename (expand-file-name user-emacs-directory)))
    ;; Dynamic scoping to the rescue
    (let ((org-confirm-babel-evaluate nil))
        (org-babel-tangle))))
(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'chema/org-babel-tangle-config)))

;; (defun chema/lsp-mode-setup ()
;;   (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
;;   (lsp-headerline-breadcrumb-mode))

;; (use-package lsp-mode
;;   :commands (lsp lsp-deferred)
;;   :hook (lsp-mode . chema/lsp-mode-setup)
;;   :init
;;   (setq lsp-keymap-prefix "C-c l")  ;; Or 'C-l', 's-l'
;;   :config
;;   (lsp-enable-which-key-integration t))

;; (use-package lsp-ui
;;   :hook (lsp-mode . lsp-ui-mode)
;;   :custom
;;   (lsp-ui-doc-position 'bottom))

;; (use-package lsp-treemacs
;;   :after lsp)

;; (use-package lsp-ivy
;;   :after lsp)

;; (use-package dap-mode
;;   ;; Uncomment the config below if you want all UI panes to be hidden by default!
;;   ;; :custom
;;   ;; (lsp-enable-dap-auto-configure nil)
;;   ;; :config
;;   ;; (dap-ui-mode 1)
;;   :commands dap-debug
;;   :config
;;   ;; Set up Node debugging
;;   (require 'dap-node)
;;   (dap-node-setup) ;; Automatically installs Node debug adapter if needed

;;   ;; Bind `C-c l d` to `dap-hydra` for easy access
;;   (general-define-key
;;     :keymaps 'lsp-mode-map
;;     :prefix lsp-keymap-prefix
;;     "d" '(dap-hydra t :wk "debugger")))

;; Paredit
 ;;(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
 ;; Smartparents 
 (require 'smartparens-config)
 (add-hook 'smartparens-enabled-hook #'evil-smartparens-mode)
 ;; (add-hook 'emacs-lisp-mode-hook       #'evil-lispy-mode)
 ;; (add-hook 'eval-expression-minibuffer-setup-hook #'evil-lispy-mode)
 ;; (add-hook 'ielm-mode-hook             #'evil-lispy-mode)
 (add-hook 'lisp-mode-hook             #'smartparens-mode)
 ;; (add-hook 'lisp-interaction-mode-hook #'evil-lispy-mode)
 ;; (add-hook 'scheme-mode-hook           #'evil-lispy-mode)

 ;; Taken from portacle
 (defun set-slime-repl-return ()
    (define-key slime-repl-mode-map (kbd "RET") 'slime-repl-return-at-end)
        (define-key slime-repl-mode-map (kbd "<return>") 'slime-repl-return-at-end))
 (defun slime-repl-return-at-end ()
        (interactive)
        (if (<= (point-max) (point))
            (slime-repl-return)
            (slime-repl-newline-and-indent)))

 (defun hyperspec-lookup--hyperspec-lookup-w3m (orig-fun &rest args)
      (let ((browse-url-browser-function 'w3m-browse-url))
      (apply orig-fun args)))

(use-package slime
  :init
;; Common Lisp implementation
(setq inferior-lisp-program "sbcl")
;; Local Hyperspec
(setq common-lisp-hyperspec-root (format "file://%s/HyperSpec/" (getenv "HOME")))
(add-hook 'slime-repl-mode-hook 'set-slime-repl-return)
(add-hook 'lisp-mode-hook
        (lambda ()
          (unless (slime-connected-p)
            (save-excursion (slime)))))
 (advice-add 'hyperspec-lookup :around #'hyperspec-lookup--hyperspec-lookup-w3m)
   :config
   (setq slime-contribs '(slime-fancy slime-asdf slime-sprof slime-mdot-fu
                       slime-compiler-notes-tree slime-hyperdoc
                       slime-indentation slime-repl))
   (setq slime-complete-symbol-function 'slime-fuzzy-complete-symbol)
   (setq slime-net-coding-system 'utf-8-unix)
   (setq slime-startup-animation nil)
   (setq slime-auto-select-connection 'always)
   (setq slime-kill-without-query-p t)
   (setq slime-description-autofocus t)
   (setq slime-fuzzy-explanation "")
   (setq slime-asdf-collect-notes t)
   (setq slime-inhibit-pipelining nil)
   (setq slime-load-failed-fasl 'always)
   (setq slime-when-complete-filename-expand t)
   (setq slime-repl-history-remove-duplicates t)
   (setq slime-repl-history-trim-whitespaces t)
   (setq slime-export-symbol-representation-auto t)
   (setq lisp-indent-function 'common-lisp-indent-function)
   (setq lisp-loop-indent-subclauses nil)
   (setq lisp-loop-indent-forms-like-keywords t)
   (setq lisp-lambda-list-keyword-parameter-alignment t))

    ;; Make sure Slime knows about our SBCL
    ;;(setq slime-lisp-implementations
          ;;`((sbcl (,(portacle-bin-path "sbcl")))))

    ; Make sure Slime stores the FASLs within Portacle
    ;; @Override
    ;;(defun slime-init-command (port-filename _coding-system)
    ;;  "Return a string to initialize Lisp."
    ;;  (let ((loader (if (file-name-absolute-p slime-backend)
    ;;                    slime-backend
    ;;                    (concat slime-path slime-backend))))
    ;;    ;; Return a single form to avoid problems with buffered input.
    ;;    (format "%S\n\n"
    ;;            `(progn
    ;;               (load ,(slime-to-lisp-filename (expand-file-name loader))
    ;;                     :verbose t)
    ;;               (setf (symbol-value (read-from-string "swank-loader:*fasl-directory*"))
    ;;                     ,(slime-to-lisp-filename (portacle-app-path "asdf" "cache/swank/")))
    ;;               (funcall (read-from-string "swank-loader:init"))
    ;;               (funcall (read-from-string "swank:start-server")
    ;;                        ,(slime-to-lisp-filename port-filename))))))

    ;; Make sure we don't clash with SLY
    ;;(defun portacle--resolve-ide-conflict (new-hook
    ;;                                       old-hook)
    ;;  "Replace OLD-HOOK with NEW-HOOK in `lisp-mode-hook'.
    ;;Also re-issue `lisp-mode' in every Lisp source buffer so that SLIME
    ;;or SLY are suitably setup there."
    ;;  (remove-hook 'lisp-mode-hook old-hook)
    ;;  (add-hook 'lisp-mode-hook new-hook t)
    ;;  (mapc (lambda (buf)
    ;;          (with-current-buffer buf
    ;;            (when (eq major-mode 'lisp-mode)
    ;;              ;; XXX: actually our own *scratch* is special because
    ;;              ;; re-issuing lisp-mode there would drown out the pretty
    ;;              ;; buttons.
    ;;              (unless (equal "*scratch*" (buffer-name))
    ;;                (lisp-mode)))))
    ;;        (buffer-list)))
    ;;
    ;;(advice-add 'slime :before
    ;;            (lambda (&rest ignored)
    ;;              (portacle--resolve-ide-conflict 'slime-lisp-mode-hook
    ;;                                              'sly-editing-mode))
    ;;            '((name . portacle-advice-before-slime)))

(add-to-list 'load-path (expand-file-name (concat (file-name-as-directory "pde") "lisp") user-emacs-directory))
 (load "pde-load")
;; (add-hook 'cperl-mode-hook
;;           (setq electric-pair-mode t)
;;           (lambda ()
;;             (define-key cperl-mode-map (kbd "C-x ñ") 'perldoc)))

(add-to-list 'load-path "/usr/bin")

(use-package company
     ;; :after lsp-mode
     ;; :hook (lsp-mode . company-mode)
     :bind (:map company-active-map
                 ("<tab>" . company-complete-selection)
                 ("C-h" . nil))
    ;; (:map lsp-mode-map
          ;; ("<tab>" . company-indent-or-complete-common))
     :init
     (add-hook 'after-init-hook 'global-company-mode)
     :custom
     (company-minimum-prefix-length 1)
     (company-idle-delay 0.0))

  ;;(use-package company-dict
  ;;:config
  ;; Where to look for dictionary files. Default is ~/.emacs.d/dict
  ;;    (setq company-dict-dir (expand-file-name "dict/" user-emacs-directory)))
  ;;  ;; Optional: if you want it available everywhere
  ;;  (add-to-list 'company-backends 'company-dict))

  (use-package company-box
    :hook (company-mode . company-box-mode))

(company-quickhelp-mode 1)
(setq company-quickhelp-delay 0.7
      company-tooltip-align-annotations t)

(global-company-mode)
(push 'slime-company slime-contribs)

(define-key company-active-map (kbd "<up>") 'company-select-previous)
;;(define-key company-active-map (kbd "<down>") 'company-select-next)
;;(define-key company-active-map (kbd "\C-n") 'company-select-next)
;;(define-key company-active-map (kbd "\C-p") 'company-select-previous)
;;(define-key company-active-map (kbd "\C-d") 'company-show-doc-buffer)
;;(define-key company-active-map (kbd "M-.") 'company-show-location)

  (use-package yasnippet
     :config
    (yas-reload-all)
    ;;(setq yas-snippet-dirs '("~/.emacs.d/snippets"))
    (add-hook 'prog-mode-hook #'yas-minor-mode))

(use-package projectile
  :diminish projectile-mode
  :config
  (projectile-mode)
  :custom
  ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "~/Projects/Code")
    (setq projectile-project-search-path '("~/Projects/Code")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :after projectile
  :config (counsel-projectile-mode))

(use-package magit
  :commands magit-status
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;; NOTE: Make sure to configure a GitHub token before using this package!
;; - https://magit.vc/manual/forge/Token-Creation.html#Token-Creation
;; - https://magit.vc/manual/ghub/Getting-Started.html#Getting-Started
(use-package forge
  :after magit)

(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package term
  :commands term
  :config
  (setq explicit-shell-file-name "bash") ;; Change this to zsh, etc
  ;;(setq explicit-zsh-args '())         ;; Use 'explicit-<shell>-args for shell-specific args

  ;; Match the default Bash shell prompt.  Update this if you have a custom prompt
  (setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *"))

(use-package eterm-256color
  :hook (term-mode . eterm-256color-mode))

(use-package vterm
  :commands vterm
  :config
  (setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *")  ;; Set this to match your custom shell prompt
  ;;(setq vterm-shell "zsh")                       ;; Set this to customize the shell to launch
  (setq vterm-max-scrollback 10000))

(when (eq system-type 'windows-nt)
  (setq explicit-shell-file-name "powershell.exe")
  (setq explicit-powershell.exe-args '()))

(defun chema/configure-eshell ()
  ;; Save command history when commands are entered
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)

  ;; Truncate buffer for performance
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)

  ;; Bind some useful keys for evil-mode
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "C-r") 'counsel-esh-history)
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
  (evil-normalize-keymaps)

  (setq eshell-history-size         10000
        eshell-buffer-maximum-lines 10000
        eshell-hist-ignoredups t
        eshell-scroll-to-bottom-on-input t))

(use-package eshell-git-prompt
  :after eshell)

(use-package eshell
  :hook (eshell-first-time-mode . chema/configure-eshell)
  :config

  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)
    (setq eshell-visual-commands '("htop" "zsh" "vim")))

  (eshell-git-prompt-use-theme 'powerline))

(use-package dired
    :ensure nil
    :commands (dired dired-jump)
    :bind 
    (("C-x C-j" . dired-jump))
    :custom
    ((dired-listing-switches "-agho --group-directories-first"))
    :config
    (setq dired-dwim-target t)
    (evil-collection-define-key 'normal 'dired-mode-map
      "h" 'dired-single-up-directory
      "l" 'dired-single-buffer
      "q" 'kill-this-buffer))

  (use-package dired-single
     :commands (dired dired-jump))

  (use-package all-the-icons-dired
     :hook (dired-mode . all-the-icons-dired-mode))

  ;; (use-package dired-open
  ;;   :commands (dired dired-jump)
  ;;   :config
  ;;   ;; Doesn't work as expected!
  ;;   ;;(add-to-list 'dired-open-functions #'dired-open-xdg t)
  ;;   (setq dired-open-extensions '(("png" . "feh")
  ;;                                 ("mkv" . "mpv"))))

  (use-package dired-hide-dotfiles
    :hook (dired-mode . dired-hide-dotfiles-mode)
    :config
    (evil-collection-define-key 'normal 'dired-mode-map
      "H" 'dired-hide-dotfiles-mode))

(use-package mu4e
  :ensure nil
  :config
  (setq mu4e-maildir (expand-file-name "~/Mail"))
  (setq mu4e-drafts-folder "/Drafts")
  (setq mu4e-sent-folder   "/Sent")
  (setq mu4e-trash-folder  "/Trash")
  ;;(setq mu4e-sent-folder "~/Mail/Sent"
  (setq mu4e-headers-auto-update t)                ; avoid to type `g' to update
  (setq mu4e-view-show-images t)                   ; show images in the view buffer
  (setq mu4e-html2text-command "w3m -T text/html") ; how to hanfle html-formatted emails
  (setq mu4e-use-fancy-chars t)                    ; allow fancy icons for mail threads
  (setq mu4e-sent-messages-behavior 'delete)
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-update-interval 300)
  (setq mu4e-change-filenames-when-moving t)
  ;;(setq ;;mu4e-get-mail-command "emacsclient -e '(mu4e-update-index)'"
  (setq mu4e-alert-enable-notifications t)
  (setq mu4e-alert-enable-mode-line-display t)
  ;(run-with-timer 0 (* 3 60) 'mu4e-update-index)
  ;; setup some handy shortcuts
  (setq mu4e-maildir-shortcuts
      '(("/INBOX"             . ?i)
        ("/Sent"              . ?s)
        ("/Trash"             . ?t)))
  (setq user-mail-address (getenv "MAIL_ADDRESS"))
  (setq user-full-name  (getenv "MY_FULLNAME"))
  (define-key mu4e-headers-mode-map (kbd "j") 'next-line)
  (define-key mu4e-headers-mode-map (kbd "k") 'previous-line)
  (define-key mu4e-headers-mode-map (kbd "l") 'mu4e-headers-view-message)
  (define-key mu4e-headers-mode-map (kbd "C-j") 'mu4e~headers-jump-to-maildir))

  ;;(setq message-signature
  ;;  (concat
  ;;    "Mr. Nobody\n"
  ;;    "http://www.example.com\n"))

(use-package smtpmail
  :config
  (setq send-mail-function 'smtpmail-send-it)
  (setq starttls-use-gnutls t)
  (setq smtpmail-stream-type 'starttls)
  (setq smtpmail-smtp-server (getenv "SMTP_SERVER"))
  (setq smtpmail-smtp-service (getenv "SMTP_PORT"))
  (setq smtpmail-smtp-user (getenv "MAIL_ADDRESS"))
  ;;(setq smtpmail-auth-credentials (expand-file-name "~/.authinfo.gpg"))
  (auth-source-pass-enable)
  (setq auth-sources '(password-store))
  (setq auth-source-debug t)
  (setq auth-source-do-cache nil)
  (setq smtpmail-debug-info t))

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

 (global-set-key (kbd "C-h") 'delete-backward-char)
 (global-set-key (kbd "C-k") 'crux-smart-kill-line)
 (global-set-key (kbd "C-ñ") 'undo-fu-only-undo)
 (global-set-key (kbd "C-r") 'swiper-backward)
 ;(global-set-key (kbd "C-z") 'er/expand-region)

 ;; C-c
 (global-set-key (kbd "C-c /") 'counsel-ag)
 (global-set-key (kbd "C-c a") 'org-agenda)
 (global-set-key (kbd "C-c b") 'org-switchba)
 (global-set-key (kbd "C-c c") 'org-capture)
 (global-set-key (kbd "C-c d") 'crux-duplicate-current-line-or-region)
 (global-set-key (kbd "C-c e") 'crux-eval-and-replace)
 (global-set-key (kbd "C-c f") 'find-file-in-project)
 (global-set-key (kbd "C-c g") 'mc/edit-lines)
 ;;(global-set-key (kbd "C-c i") 'freetouse)
 (global-set-key (kbd "C-c k") 'kill-this-buffer)
 (global-set-key (kbd "C-c l") 'org-store-link)
 (global-set-key (kbd "C-c m") 'switch-to-last-buffer)
 (global-set-key (kbd "C-c n") 'next-buffer)
 (global-set-key (kbd "C-c o") 'find-file-at-point)
 (global-set-key (kbd "C-c p") 'previous-buffer)
 ;(global-set-key (kbd "C-c q") 'freetouse)
 ;(global-set-key (kbd "C-c r") 'freetouse)
 (global-set-key (kbd "C-c s") 'crux-visit-shell-buffer)
 (global-set-key (kbd "C-c t") 'crux-visit-term-buffer)
 ;;  (global-set-key (kbd "C-c u") 'crux-view-url)
 (global-set-key (kbd "C-c w") 'mc/mark-all-words-like-this)
 ;(global-set-key (kbd "C-c x") 'freetouse)
 (global-set-key (kbd "C-c y") 'mc/mark-all-like-this)
 (global-set-key (kbd "C-c z") 'shell)
 ;; C-c C-key
 ;;(global-set-key (kbd "C-c C-c") 'freetouse)
 (global-set-key (kbd "C-c C-d") 'crux-insert-date)
 ;;(global-set-key (kbd "C-c C-e") 'crux-eval-and-replace)
 (global-set-key (kbd "C-c C-i") (lambda () (interactive)
                                   (find-file "~/.emacs.d/emacs.org")))
 ;;(global-set-key (kbd "C-c C-k") 'crux-kill-whole-line)
 (global-set-key (kbd "C-c C-l s") 'lorem-ipsum-insert-sentences)
 (global-set-key (kbd "C-c C-l p") 'lorem-ipsum-insert-paragraphs)
 (global-set-key (kbd "C-c C-l l") 'lorem-ipsum-insert-list)
 (global-set-key (kbd "C-c C-r") 'find-file-read-only)
 ;;(global-set-key (kbd "C-c C-s") 'freetouse)
 (global-set-key (kbd "C-c C-t") 'crux-transpose-windows)
;; C-x key
;; (global-set-key (kbd "C-x c") 'freetouse)
(global-set-key (kbd "C-x j") 'join-line)
(global-set-key (kbd "C-x k") 'kill-buffer)
(global-set-key (kbd "C-x o") 'crux-recentf-find-file)
(global-set-key (kbd "C-x p") 'crux-reopen-as-root-mode)
;;(global-set-key (kbd "C-x u") 'mu4e)
;;(global-set-key (kbd "C-x w") 'freetouse)
(global-set-key (kbd "C-x x") 'ispell)
;;(global-set-key (kbd "C-x y") 'freetouse)

;; C-x C-key

;; M-key
(defun lookup-word (word)
  (interactive (list (save-excursion (car (ispell-get-word nil)))))
  (browse-url (format "https://dle.rae.es/%s" word)))
(global-set-key (kbd "M-#") 'lookup-word)
(global-set-key (kbd "M-SPC") 'delete-horizontal-space)
(global-set-key (kbd "M-\\") 'just-one-space)
;(global-set-key (kbd "M-i") 'imenu)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-p") 'backward-paragraph)
(global-set-key (kbd "M-n") 'forward-paragraph)
(global-set-key (kbd "M-ñ") 'mark-word)

;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))
