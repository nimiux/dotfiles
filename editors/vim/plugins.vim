" Vim plugin that allows use of vifm as a file picker
" Plugin: 'vifm/vifm.vim'
" mirror of vim plugin for the excellent vifm file manager
" Plugin: 'istib/vifm.vim'

" Plugin: bling/vim-airline
" lean & mean status/tabline for vim that's light as air
" unicode symbols
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
"let g:airline_powerline_fonts = 1
"let g:airline_theme='powerlineish'
"let g:airline_detect_modified = 1
"let g:airline_detect_paste = 1
"let g:airline_detecdt_iminsert = 0
"let g:airline_exclude_preview = 1
"let g:airline_inactive_collapse = 1
"let g:airline_left_sep=''
"let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
"let g:airline_right_sep=''
"let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
"let g:airline_section_z=''
"let g:airline_symbols.linenr = '␊'
"let g:airline_symbols.linenr = '¶'
let g:airline_symbols.linenr = '␤'
"let g:airline_symbols.branch = '⎇'
"let g:airline_symbols.paste = 'ρ'
"let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = '▌'
let g:airline#extensions#tabline#left_alt_sep = '▌'"}}}

" Plugin: Shougo/neocomplcache
" Next generation completion framework after neocomplcache
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_auto_select = 1
let g:neocomplete#enable_smart_case = 1
let g:neocomplete#auto_completion_start_length = 2
" fuzzy completion breaks dot-repeat more noticeably
" https://github.com/Shougo/neocomplete.vim/issues/332
let g:neocomplete#enable_fuzzy_completion = 0
" let g:neocomplete#max_list
" let g:neocomplete#max_keyword_width
" let g:neocomplete#manual_completion_start_length
" let g:neocomplete#min_keyword_length
" let g:neocomplete#enable_ignore_case
" let g:neocomplete#enable_camel_case
" let g:neocomplete#disable_auto_complete
" let g:neocomplete#enable_cursor_hold_i
" let g:neocomplete#cursor_hold_i_time
" let g:neocomplete#enable_auto_delimiter
" let g:neocomplete#enable_refresh_always
" let g:neocomplete#enable_multibyte_completion
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'
" let g:neocomplete#lock_iminsert
"let g:neocomplete#data_directory = '~/.vim/tmp/neocomplete'
" let g:neocomplete#keyword_patterns
"if !exists('g:neocomplete#keyword_patterns')
"   let g:neocomplete#keyword_patterns = {}
"endif
"let g:neocomplete#keyword_patterns['default'] = '\h\w*'
" let g:neocomplete#force_omni_input_patterns
"let g:neocomplete#same_filetypes._ = '_'
" let g:neocomplete#delimiter_patterns
" let g:neocomplete#release_cache_time
" let g:neocomplete#tags_filter_patterns
" let g:neocomplete#use_vimproc
" let g:neocomplete#skip_auto_completion_time
" let g:neocomplete#enable_auto_close_preview
" let g:neocomplete#fallback_mappings
    "\ '_' : '/home/foo/.vim/dictionaries/spanish/spanish.lex',
    "\ }
let g:neocomplete#sources#tags#cache_limit_size = 16777216 " 16MB
" let g:neocomplete#ignore_source_files
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '/home/foo/.vim/dictionaries/spanish/spanish.lex'
    \ }

" Plugin: 'SirVer/ultisnips' | Plugin: 'honza/vim-snippets'
" UltiSnips - The ultimate snippet solution for Vim"{{{
" http://fueledbylemons.com/blog/2011/07/27/why-ultisnips/
" vim-snipmate default snippets (Previously snipmate-snippets)
"let g:snips_author = "Me"
"let g:UltiSnipsSnippetDir=
"let g:UltiSnipsSnippetDirectories=['UltiSnips']
let g:UltiSnipsExpandTrigger = "<Tab>"
"let g:UltiSnipsListSnippets = "<C-Tab>"
let g:UltiSnipsJumpForwardTrigger = "<C-n>"
"let g:UltiSnipsJumpBackwardTrigger = "<C-u>"
let g:UltiSnipsEditSplit="vertical"

" A dummy text generator
" Plugin: 'vim-scripts/loremipsum'

" Fast and Easy Find and Replace Across Multiple Files
" Plugin: 'dkprice/vim-easygrep'"}}}

