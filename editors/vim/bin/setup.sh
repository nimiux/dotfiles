#!/bin/bash

PLUGIN_URI="https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"

function useVundle() {
    if [ ! -f ~/.vim/bundle/Vundle.vim/README.md ];
    then
        echo "Vundle not found, installing..."
        mkdir -p ~/.vim/bundle
        git clone git://github.com/gmarik/Vundle.vim ~/.vim/bundle/Vundle.vim
    else
        echo "Vundle found."
    fi
}

function usevimplug() {
    if [ ! -f ~/.vim/autoload/plug.vim ] ; then
        echo "vimplug not found, installing..."
        curl -fLo ~/.vim/autoload/plug.vim --create-dirs ${PLUGIN_URI}
    else
        echo "vimplug found."
    fi
}

function installplugins() {
    # Vundle
    #vim +PluginClean! +PluginInstall! +qall
    # vimplug
    vim +PlugClean! +PlugInstall! +qall
}

function updateplugins() {
    # Vundle
    #vim +PluginUpdate! +qall
    # vimplug
    vim +PlugUpdate! +qall
}

#echo "Syncing Vundle bundles..."

usevimplug
[[ -d ~/.vim/plugged ]] && updateplugins || installplugins
echo "All done!"
