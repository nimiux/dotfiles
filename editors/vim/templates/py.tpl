# <+PROJECT+>
# -*- coding: utf-8 -*-
#
# Copyright <+YEAR+> <+AUTHOR+>
#
# by $Author: $
#
# This software is governed by a license. See
# LICENSE.txt for the terms of this license.

# $Id: $

# Documentation string
__doc__="""<+DOCUMENTATION+>"""

# Version
__version__='$Revision: $'[11:-2]

#__docformat__ = "restructuredtext en"

