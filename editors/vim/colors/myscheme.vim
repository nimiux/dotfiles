set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif

"Load the 'base' colorscheme - the one you want to alter
runtime colors/default.vim

"Override the name of the base colorscheme with the name of this custom one
let g:colors_name = "my-scheme"

"Clear the colors for any items that you don't like
hi clear Comment
"hi clear StatusLineNC

"Set up your new & improved colors
hi Comment ctermfg=magenta
"hi StatusLineNC guifg=LightCyan guibg=blue gui=bold
