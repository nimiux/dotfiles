# Installation #

1. Clone or unpack this in your $HOME/.vim directory
2. Launch $HOME/.vim/bin/setup.sh
3. If you need more plugins to be included, edit plugins.vim and relaunch setup.sh

# Notes #

* To use the distinguised color scheme:

        export TERM=xterm-256color
        or
        set t_Co=256
