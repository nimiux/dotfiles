#!/bin/awk -f
# -*- mode: awk; -*-

# Copyright (c) $1-`(insert (format-time-string "%Y"))` by `(insert (user-full-name))` <`(insert (getenv "MY_DEV_MAIL_ADDRESS"))`>. All rights reserved.

# This is free and unencumbered software released into the public domain.
# For more information, please refer to <http://unlicense.org/>

$0
