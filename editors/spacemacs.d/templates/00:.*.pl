#!/usr/bin/env perl
# -*- mode: perl; -*-

# Version: 0.01

use warnings;
use strict;

$0

__END__

=head1 NAME

`(insert (file-name-nondirectory (buffer-file-name)))` - Describe the usage of script briefly

=head1 SYNOPSIS

 [options] args

      -opt --long      Option description

=head1 DESCRIPTION

Stub documentation.

=head1 AUTHOR

`(insert (user-full-name))` <`(insert (getenv "MY_DEV_MAIL_ADDRESS"))`>.

=head1 COPYRIGHT AND LICENSE

Copyright (c) $1-`(insert (format-time-string "%Y"))` by `(insert (user-full-name))` <`(insert (getenv "MY_DEV_MAIL_ADDRESS"))`>. All rights reserved.

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

=cut
