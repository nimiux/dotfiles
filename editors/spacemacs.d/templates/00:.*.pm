# -*- mode: perl; -*-
package $1;

use strict;
use warnings;

use Carp;


1;
__END__

=head1 NAME

`(insert (file-name-nondirectory (buffer-file-name)))` - Perl extension for..

=head1 SYNOPSIS

   use ...;

=head1 DESCRIPTION

Stub documentation.

=head2 EXPORT

None by default.

=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

`(insert (user-full-name))` <`(insert (getenv "MY_DEV_MAIL_ADDRESS"))`>.

=head1 COPYRIGHT AND LICENSE

Copyright (c) $1-`(insert (format-time-string "%Y"))` by `(insert (user-full-name))` <`(insert (getenv "MY_DEV_MAIL_ADDRESS"))`>. All rights reserved.

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

=cut
