#!/bin/bash

mygit () {
    project="$1"
    shift
    command="$1"
    shift
    options="$*"
    git -C $project $command $options
}

message="$*"
while [[ -z "$message" ]] ; do
   echo -n "No message provided, please type one: "
   read message 
done
for project in ${MYREPOSPATH}/* ; do
   echo -n "Proyecto $project: "
   if [[ -z $(mygit $project status --porcelain --untracked-files=yes) ]] ; then
      echo "No changes"
   else
      (cd $project ; git add . ; git commit -am "$message" ; git push)
   fi
done
