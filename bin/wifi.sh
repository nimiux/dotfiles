#!/bin/sh
status=$(nmcli radio wifi)
if [ "$status" == "enabled" ] ; then
    echo -n "%{F#FF2483}"
    echo "🛜"
else
    echo -n "%{F#F0C674}"
    echo "🚫"
fi

