#!/usr/bin/perl
# powermenu.pl --- Power menu
# Created: 12 May 2022
# Version: 0.01

use strict;
use warnings;
use utf8;
use feature ':5.34';
use Encode qw (decode_utf8);

use Data::Dumper;
#open FILE, ">>", "/tmp/kk";

no warnings 'utf8';
@ARGV = map { decode_utf8($_, 1) } @ARGV;

my @visibleentries = ("lock", "logout", "reboot", "suspend", "hibernate", "shutdown");

my $menu = {
    lock => {
        "text"     => "lock screen",
        "icon"     => "\x{f023}",
        "action"   => 'loginctl lock-session ${XDG_SESSION_ID-}',
        "ask"      => 0,
        "visible"  => 1,
    },
    logout => {
        "text"     => "log out",
        "icon"     => "\x{f842}",
        "action"   => 'loginctl terminate-session ${XDG_SESSION_ID-}',
        "ask"      => 0,
        "visible"  => 1,
    },
    suspend => {
        "text"     => "suspend",
        "icon"     => "\x{f9b1}",
        "action"   => 'systemctl suspend',
        "ask"      => 0,
        "visible"  => 1,
    },
    hibernate => {
        "text"     => "hibernate",
        "icon"     => "\x{f7c9}",
        "action"   => 'systemctl hibernate',
        "ask"      => 0,
        "visible"  => 1,
    },
    reboot => {
        "text"     => "reboot",
        "icon"     => "\x{fc07}",
        "action"   => 'systemctl reboot',
        "ask"      => 1,
        "visible"  => 1,
    },
    shutdown => {
        "text"     => "shutdown",
        "icon"     => "\x{f011}",
        "action"   => 'systemctl poweroff',
        "ask"      => 1,
        "visible"  => 1,
    },
    cancel => {
        "text"     => "cancel",
        "icon"     => "\x{00d7}",
        "action"   => 'cancel',
        "ask"      => 0,
        "visible"  => 0,
    },
    switch => {
        "text"     => "Switch user",
        "icon"     => "\x{f518}",
        "action"   => 'switchuser',
        "ask"      => 0,
        "visible"  => 0,
    },
};

sub renderentry {
    my ($key, $hash, $prefix) = @_;
    $prefix = $prefix || "";
    my $entry = $hash->{$key};
    my $icon = $entry->{icon};
    my $text = "\u$entry->{text}";
    my $iconmarkup = qq|<span font_size="medium">$icon</span>|;
    my $textmarkup= qq|<span font_size="medium">$prefix$text</span>|;
    #"\x{200E}$iconmarkup \x{2068}$textmarkup\x{2069}\n";
    "\x{200E}$iconmarkup $textmarkup";
}

foreach (keys %$menu) {
    $menu->{$_}->{entry} = renderentry($_, $menu);
    $menu->{$_}->{confirmation} = renderentry($_, $menu, "Yes, ");
}

#my @visibleentries = grep { $menu->{$_}->{"visible"}; } keys %$menu;
#print FILE Dumper(\@visibleentries);
my @entries = map { $menu->{$_}->{entry}; } @visibleentries;
my $entries = join "\n", @entries;
#print FILE Dumper(\@entries);
#print FILE $entries;

sub showmenu {
    my ($entries) = @_;
    # Don't allow custom entries
    say "\x{00}no-custom\x{1f}true";
    # Use markup
    say "\x{00}markup-rows\x{1f}true";
    # Prompt
    say "\x{00}prompt\x{1f}Power menu";
    say $entries;
}

sub askforconfirmation {
    my ($entry, $cancel) = @_;
    if ($entry->{ask}) {
        say "\x{00}prompt\x{1f}Are you sure";
        say $entry->{confirmation};
        say $cancel->{entry};
        die;
    }
}

sub main {
    my $selection = join "", @ARGV;

    #print FILE "SELECTION IS : $selection\n";
    my ( $currententry, $currentaction );
    if ($selection) {
        foreach ( keys %$menu ) {
            $currententry = $menu->{$_};

            #print FILE "Current entry is: $currententry->{'entry'}\n";
            if ( $currententry->{entry} eq $selection ) {
                askforconfirmation $currententry, $menu->{cancel};
                system "$currententry->{action}";
            }
            if ( $currententry->{confirmation} eq $selection ) {
                system "$currententry->{action}";
            }
        }
    }
    else {
        showmenu $entries;
    }
}

main();

__END__

=head1 NAME

powermenu.pl - This script shows a powermenu suitable for rofi

Example:
   rofi -show powermenu -modi powermenu:powermenu.pl " -theme Paper -font "Feather 14" -width 15 -lines 6

=head1 SYNOPSIS

powermenu.pl

=head1 DESCRIPTION

Power menu generator

=head1 AUTHOR

José María Alonso <lt>nimiux@freeshell.de<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2022-2025 by José María Alonso <nimiux@freeshell.de>

This progmram is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

Report bugs at:

https://gitlab.com/nimiux/dotfiles/issues

=cut
