#!/bin/bash

TOUCHPADID=11
enabled=$(xinput list-props ${TOUCHPADID} | grep 'Device Enabled' | cut -d\: -f 2 | sed 's/\t//')
echo $enabled
action=$([ $enabled -eq 1 ] && echo "disable" || echo "enable")
echo $action
xinput $action ${TOUCHPADID}

