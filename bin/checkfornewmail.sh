#!/bin/sh

NOTIFY_ICON="/usr/share/icons/Adwaita/32x32/status/mail-read-symbolic.symbolic.png"
NEWMAILDIR="${HOME}/Mail/Inbox/new"

while : ; do
    inotifywait -e moved_to $NEWMAILDIR
    notify-send -u low -i $NOTIFY_ICON "You have mail"
done &
echo $! > /tmp/checkfornewmail.pid
