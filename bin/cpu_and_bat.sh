#!/bin/sh

cpuwarning=80
batterywarning=10
checkinterval=120

notify () {
    #espeak --stdout "${@}" | aplay -
    #espeak "${@}"
    #re say_fortune ${@} &
    # dunstify -u critical "${@}"
    #notify-send -u critical -i $NOTIFY_ICON "${@}"
    notify-send -u critical "${@}"
}

while : ; do
        cpuuse="$(re.pl cpu_usage)"
	batterycharge=`upower -i $(upower -e | grep BAT) | grep --color=never 'percentage' | awk -e '{print $2}' | sed -e 's/%//'`
        #echo "$(date) CPU at $cpuuse. Battery at $batterycharge%" >> /tmp/cpu_and_bat.log
	if [[ $cpuuse -gt $cpuwarning ]] ; then
            notify "  CPU warning: $cpuuse%"
        fi
	if [[ $batterycharge -le $batterywarning ]] ; then
            notify "  Battery warning: $batterycharge%"
        fi
        sleep $checkinterval
done &
echo $! > /tmp/cpu_and_bat.pid
