# Useful functions and aliases

# Archlinux
pacmanlsfiles () {
    pacman -Qlq "${1}" | grep -v '/$' | xargs du -h | sort -h
}
alias pacmanfindpackage='pacman -Qo'
alias pacmancleancache='sudo pacman -Scc'

set -f pacmanlsfiles

# Commands
alias r='fc -e : -1'
alias cd-='cd $(dirname $1)'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias 1='cd -'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias 7='cd -7'
alias 8='cd -8'
alias 9='cd -9'
alias df='df -H'
alias l='ls -lrtBh --author --color=always --group-directories-first'
alias la='l -a'
#alias ll='tyls'
alias mounts='mount | column -t'
alias path='echo -e ${PATH//:/\\n}'
alias ports='netstat -tulanp'
alias now='date +"%T"'
alias nowdate='date +"%d-%m-%Y"'
alias nowtime=now
alias poff='sudo poweroff'
alias reboot='sudo reboot'
alias root='/usr/bin/sudo -i'
alias sha1='openssl sha1'
alias sha256='openssl sha256'
alias vi='vim'
alias mount-cloud='mount ${CLOUDMOUNT}'
alias umount-cloud='umount ${CLOUDMOUNT}'
alias eject='sudo eject'

# Repos aliases
alias repos='re.pl repos'
alias update-repos='repos update'
alias update-my-repos='repos update-mine'
alias status-repos='repos status'

# Proxy
alias proxy='ssh -CfND 8080 '
alias pproxy='pgrep "ssh -D 8080"'

# Remote
alias filladhoo='ssh filladhoo'
alias filladhoo-wired='ssh filladhoo-wired'
alias raspberry='ssh raspberry'
alias remoteshell='ssh remoteshell'
alias remoteproxy="ssh -fCNT -L 8080:localhost:$PROXYPORT remoteshell"
alias remotelisp='ssh -fCNT -L localhost:4005:localhost:4005 remoteshell'
alias socksproxy='ssh -fCNT -D $SOCKSPORT remoteshell '

# iptables
alias ipt='sudo /sbin/iptables'
alias iptlist='sudo /sbin/iptables -L -n -v --line-numbers'
alias iptlistin='sudo /sbin/iptables -L INPUT -n -v --line-numbers'
alias iptlistout='sudo /sbin/iptables -L OUTPUT -n -v --line-numbers'
alias iptlistfw='sudo /sbin/iptables -L FORWARD -n -v --line-numbers'

# Applications
#alias desktop='xinit startlxde -- /etc/X11/xinit/xserverrc :1 vt2'
#alias ff='firefox &'
#alias gfu='git fetch upstream'
#alias sbcl='sbcl --disable-debugger'
alias cpan="rlwrap cpan"
alias desktop='/usr/bin/startx /usr/bin/lxsession -- :1'
alias di='espeak -v es'
alias ed='emacsclient -create-frame --alternate-editor=""'
alias hdmimplayer='mplayer -ao alsa:device=hw=0.3'
alias lisp='sbcl --eval "(require :slynk)" --eval "(slynk:create-server :dont-close t)"'
alias lo='loffice'
alias maxima='rlwrap maxima'
alias ngdu='ncdu'
alias perlrepl='rlwrap perlrepl'
alias reply='rlwrap reply'
alias sphone="blueman-sendto --device=$PHONEMAC"
alias tsw='tmux switch -t'
alias x48='x48 -connFont fixed -smallFont fixed -mediumFont fixed -largeFont fixed'
alias xemacs='/usr/bin/startx ~/.xinitrcemacs -- :2 ; logout'
