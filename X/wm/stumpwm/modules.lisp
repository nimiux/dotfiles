(in-package :stumpwm)
(set-module-dir "/usr/share/common-lisp/source/stumpwm-contrib")
;(load-module "stumptray")
;(load-module "alert-me")
;; net

(load-module "hostname")
(load-module "cpu")
(load-module "mem")
;(load-module "wifi")
(load-module "battery-portable")
(load-module "maildir")

(load-module "screenshot")

(setf *maildir-update-time* 10)
;(setf  *maildir-path* "/tmp/maildir")
