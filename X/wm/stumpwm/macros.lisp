;(in-package :stumpwm)

;;; Helper macros
;; replace a hook
(defmacro replace-hook (hook fn)
  `(remove-hook ,hook ,fn)
  `(add-hook ,hook ,fn))

;; web jump (works for Google and Imdb)
(defmacro make-web-jump (name prefix)
  `(defcommand ,name (search) ((:rest ,(concatenate 'string name " search: ")))
               (substitute #\+ #\Space search)
               (run-shell-command (concatenate 'string ,prefix search))))

