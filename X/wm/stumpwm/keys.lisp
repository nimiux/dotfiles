;(in-package :stumpwm)

;;; Key maps
;(defparameter *shift-map* (make-sparse-keymap))
;(define-key *shift-map* (kbd "@") "@")

;;; Maps bindings
;(define-key *root-map* (kbd "ISO_Level3_Shift") '*shift-map*)

; top map
;; window list
;(define-key *top-map* (kbd "C-i") "frame-windowlist")
;; next frame
;(define-key *top-map* (kbd "C-k") "fother")
;; volume down
(define-key *top-map* (kbd "C-M-d") "lower-master-volume")
;; volume up
(define-key *top-map* (kbd "C-M-u") "raise-master-volume")
;; Rotate screen
;(define-key *top-map* (kbd "C-M-r") "rotate-screen")
;; End StumpWM
(define-key *top-map* (kbd "C-M-Delete") "quit")

;; root map
;; next in frame
(define-key *root-map* (kbd "C-space") "next-in-frame")
;; 3ddesktop
;;(define-key *root-map* (kbd "C-space") "run-shell-command 3ddesk")
;; zsh
(define-key *root-map* (kbd "c") "run-shell-command terminology -e zsh")
;; help map
(define-key *root-map* (kbd "d") '*help-map*)
;; editor
(define-key *root-map* (kbd "e") "run-shell-command terminology -e $HOME/bin/edit")
;; vifm
(define-key *root-map* (kbd "E") "run-shell-command terminology -e vifm")
;; file browser
(define-key *root-map* (kbd "C-e") "run-shell-command dbus-launch pcmanfm")
;; firefox
(define-key *root-map* (kbd "f") "run-shell-command firefox")
;; chromium
(define-key *root-map* (kbd "C-f") "run-shell-command chromium")
; other frame
;; FREE
;;(define-key *root-map* (kbd "g") "")
;; goto left group
(define-key *root-map* (kbd "h") "run-shell-command 3ddesk --gotoleft")
;; Previous group
(define-key *root-map* (kbd "C-h") "gprev")
;; FREE
;;(define-key *root-map* (kbd "C-i") "")
;; move window to previous group
(define-key *root-map* (kbd "j") "gprev-with-window")
;; get help
;;(define-key *root-map* (kbd "H") "")
;; Kill group
(define-key *root-map* (kbd "k") "gkill")
;; FREE
;;(define-key *root-map* (kbd "C-k") "")
;; move window to the right
(define-key *root-map* (kbd "l") "run-shell-command 3ddesk --gotoright")
;; Move to next group
(define-key *root-map* (kbd "C-l") "gnext-with-window")
;; mutt
(define-key *root-map* (kbd "m") "run-shell-command terminology -e mymail")
;; move window up
(define-key *root-map* (kbd "n") "gnew")
;; next group
(define-key *root-map* (kbd "C-n") "gnext")
;; next window in frame
(define-key *root-map* (kbd "ntilde") "run-shell-command 3ddesk")
;; next window
(define-key *root-map* (kbd "C-ntilde") "other-in-frame")
;; Next frame
(define-key *root-map* (kbd "o") "fnext")
;; libreoffice
(define-key *root-map* (kbd "C-o") "run-shell-command loffice")
;; previous window
(define-key *root-map* (kbd "p") "gprev")
;; reload configuration
(define-key *root-map* (kbd "r") "loadrc")
;; FREE
;(define-key *root-map* (kbd "s") "")
;; terminal
(define-key *root-map* (kbd "t") "run-shell-command terminology -e tmux")
;; test
(define-key *root-map* (kbd "x") "exec xtrlock")

;; SSH somewhere
;(define-key *root-map* (kbd "C-s") "colon1 exec xterm -e ssh ")

;; More
;(define-key *top-map* (kbd "M-Tab") "pull-hidden-next")
;(define-key *top-map* (kbd "M-ISO_Left_Tab") "pull-hidden-previous") ;; shift
;(define-key *top-map* (kbd "M-]") "pull-hidden-next")
;(define-key *top-map* (kbd "M-[") "pull-hidden-previous")
;(define-key *top-map* (kbd "s-Tab") "fnext")
;(define-key *top-map* (kbd "s-]") "gnext")
;(define-key *top-map* (kbd "s-[") "gprev")
;(define-key *top-map* (kbd "s-b") "move-focus left")
;(define-key *top-map* (kbd "s-n") "move-focus down")
;(define-key *top-map* (kbd "s-p") "move-focus up")
;(define-key *top-map* (kbd "s-f") "move-focus right")
;(define-key *top-map* (kbd "M-F4") "kill")
;(define-key *top-map* (kbd "s-s") "fullscreen")
;(define-key *top-map* (kbd "M-F2") "exec")
;(define-key *top-map* (kbd "s-SPC") "exec")

;; C-t M-s is a terrible binding, but you get the idea.
;(define-key *root-map* (kbd "M-s") "google")
;(define-key *root-map* (kbd "i") "imdb")

;;; Keysyms
;; (define-keysym #x1008ff31 "XF86AudioPause")
;; (define-keysym #x1008ff15 "XF86AudioStop")
;; (define-keysym #x1008ff17 "XF86AudioNext")
;; (define-keysym #x1008ff16 "XF86AudioPrev")
;; (define-keysym #x1008ff87 "XF86Video")
