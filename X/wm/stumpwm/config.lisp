;(in-package :stumpwm)

;;; Prefix Key
;(set-prefix-key (kbd "Super_L"))
;(set-prefix-key (kbd "C-space"))
;(set-prefix-key (kbd "s-t"))
;(set-prefix-key (kbd "C-comma"))
;(set-prefix-key (kbd "ISO_Level3_Shift"))
;(set-prefix-key (kbd "Menu"))
(set-prefix-key (kbd "C-ntilde"))

;; Parameters
;(defparameter STARTUP-MESSAGE "Never stop Hacking!")
;(defparameter FOREGROUND-COLOR "white")
;(defparameter BACKGROUND-COLOR "black")
;(defparameter BORDER-COLOR "green")
;; path for background images
;(defvar *background-image-path* "/home/dummy/wp/")

;;; Startup message
;(setf *startup-message* nil)
;(setf *startup-message* STARTUP-MESSAGE)
(setf *startup-message* (machine-instance))

;;; Shell
;(setf *shell-program* (stumpwm::getenv "SHELL")) ;getenv is not exported

;;; Debug
;; stumpwm crashes or freezes too much. If we set the debug up to ludicoursly
;; high levels, maybe we'll learn something.
;(setf stumpwm::*debug-level* 10)

;;; Screens
;; run-or-raise work through multiple screens
;; (setf *run-or-raise-all-screens* 't)

;;; Groups
(defvar *groups* '(east north west south))
(defvar *first-group* (car *groups*))
(setf *default-group-name* (string-capitalize *first-group*))
;;(grename *first-group*)
;;%n: number
;;%s: status
;;%t: name
;(setf *group-format* )

;;; Frames
;(defvar *maximized-frame-size* (make-frame :number -1 :x 0 :y 0 :width 1024 :height 768))
;(defvar *current-frame-size* (make-frame :number -1 :x 0 :y 0 :width 0 :height 0))
;(defvar *current-maximized-frame* nil)
;(set-frame-outline-width 8)
;; I just don't like zero indexing frames/windows. 0 is not next to 1 on the keyboard!
;; See <http://lists.gnu.org/archive/html/stumpwm-devel/2006-08/msg00002.html>
;(setf *frame-number-map* "1234567890") ;doesn't seem to work right now
(setf *frame-number-map* "jkluio789")

;;; Windows
;; appearance
;; the width in pixels given to the borders of regular windows.
;(setf *normal-border-width* 3)
;; the width in pixels given to the borders of windows with maxsize or ratio hints.
;(setf *maxsize-border-width* 5)
;; the width in pixels given to the borders of transient or pop-up windows.
;(setf *transient-border-width* 5)
;; Window border colors
;(setf *focus-color* "gold")
;(setf *unfocus-color* "magenta")
;%n: number
;%s: * means current window, + means last window, and - means any other window.
;%t: name
;%c: class
;%i: resource ID
;%m: draw a # if the window is marked
; a prefix number can be used to crop the argument to a specified size. For instance, `%20t' crops the window's title to 20 characters.
(setf *window-format* "[%n%s%30t]")
(setf *window-info-format* (format nil "^>^B^5*%c ^b^6*%w^7*x^6*%h^7*~%%t"))
; :title
; :class
; :resource-name
;(setf *window-name-source* )
(setf *window-border-style* :none)

;;; Borders
;(setf *maxsize-border-width* 0)
;(setf *transient-border-width* 0)
;(setf *normal-border-width* 0)
;; to update on the fly:
;(dolist (s *screen-list*) (mapc 'sync-all-frame-windows (screen-groups s)))

;;; Mode Line
(setf ;*startup-mode-line* t
      *mode-line-position* :top
      *mode-line-timeout* 5
;     *mode-line-screen-position* :bottom
;     *mode-line-frame-position* :bottom
;     *mode-line-background-color* "white"
;     *mode-line-foreground-color* "forestgreen"
;     *mode-line-border-color* BORDER-COLOR
;     *mode-line-border-width* 0
;     *mode-line-pad-x* 0
;     *mode-line-pad-y* 0
)
(setf *screen-mode-line-format*
      ;;      (list '(:eval (run-shell-command "date '+%R %d/%m/%y %a' | tr -d [:cntrl:]" t))
      ;;" | | %c %t | [^B%n^b] %W"))
      (list "%h " '(:eval (run-shell-command "date '+%R, %F %a'|tr -d [:cntrl:]" t)) " | %N | %B | %c %t | [^B%n^b] %W"))
;(setf) *screen-mode-line-format*
;(list "[%w]   | "
;'(:eval (run-shell-command "ifconfig|grep inet|awk '{print $3}'|head -1 " t)))
;; Show time, cpu usage and network traffic in the modeline
;(setf *screen-mode-line-format*
;;        (list "%w | " '(:eval (run-shell-command "date" t))))
;;Specifically, I want mode line to display the result of having the shell create a string
;;of the concatenation of a space and the output of the 'date' program.
;;(setf *screen-mode-line-format*
;;      (list "%w|%g|"
;;            '(:eval (stumpwm:run-shell-command "date +%_I:%M:%S%p--%e-%a|tr -d [:cntrl:]" t))))
;; *screen-mode-line-format* '("^5*" (:eval (time-format "%k:%M")) " ^2*%n ")
;; *screen-mode-line-format* '((:eval (format nil " ^2*foo~%bar")))  ; multiline mode-line

;;; Pointer
;; use xfd -fn cursor to see cursors
;(setf *grab-pointer-foreground* (lookup-color (current-screen) "DeepSkyBlue"))
(setf
 *grab-pointer-character* 56
 *grab-pointer-character-mask* 57
 *grab-pointer-foreground* (xlib:make-color :red 0.24 :green 0.70 :blue 0.44)
 *grab-pointer-background* (xlib:make-color :red 0.95 :green 0.78 :blue 0.1))
;(setf stumpwm::*grab-pointer-font* "fixed")

;;; Mouse
;; the mouse focus policy decides how the mouse affects input focus
;; :ignore :sloppy: click
(setf *mouse-focus-policy* :click)

;; stump doesn't set a default cursor, so we set it ourselves.
;;set mouse pointer to left arrow
(run-shell-command "xsetroot -cursor_name left_ptr")

;;; Messages
;; in seconds, how long a message will appear for. This must be an integer.
;(setf *timeout-wait* 10)
;; set the message and input box position
;; :top :top-right :center...
(setf *message-window-gravity* :top-right)

;;; Time
(setf *time-format-string-default* (format nil "^5*%H:%M:%S~%^2*%A~%^7*%d %B"))


