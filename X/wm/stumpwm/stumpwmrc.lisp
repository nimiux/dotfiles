;;;;
;;;; My StumpWM configuration file
;;;;

(in-package :stumpwm)
(setf *default-package* :stumpwm)

(defparameter *stumpwm-directory* ".stumpwm"
  "Include directory")
(defvar *includes-directory*
  (directory-namestring
   (truename (merge-pathnames (user-homedir-pathname)
                              *stumpwm-directory*)))
  "Full path to includes")

(defun include (filename)
  "Loads lisp filename from the includes directory"
  (let ((file (merge-pathnames (concat (string-downcase ( symbol-name filename)) ".lisp")
                               *includes-directory*)))
    (if (probe-file file)
        (load file)
        (message "File '~a' not found" file))))

(include :modules)
(include :swank)
(include :config)
(include :macros)
(include :functions)
(include :commands)

(include :windows)
(include :fonts)
(include :modeline)
(include :keys)
(include :hooks)
(include :init)
