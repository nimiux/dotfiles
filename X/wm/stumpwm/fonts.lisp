;(in-package :stumpwm)

;;; Fonts
(set-font "-dec-terminal-medium-r-normal--14-140-75-75-c-80-iso8859-1")
(set-font "-b&h-lucida-bold-r-normal-sans-12-0-100-100-p-0-iso8859-15")
(set-font "-b&h-lucidatypewriter-bold-r-normal-sans-14-100-75-75-m-60-iso8859-15")
(set-font "kana14")
;;Set the font for the input box. I have a nifty Lisp machine font.
(set-font "-lispm-fixed-medium-r-normal-*-13-*-*-*-*-*-*-*")
;; (set-font "-artwiz-snap-normal-*-*-*-*-*-*-*-*-*-iso10646-*")
;; (set-font "-*-aqui-medium-r-*-*-11-*-*-*-*-*-*-*")
;; (set-font "-*-wenquanyi bitmap song-medium-r-normal-*-12-*-*-*-*-*-iso10646-*")
;; (set-font "-xos4-*-medium-r-normal-*-13-*-*-*-*-*-iso10646-*")
;; (set-font "-*-profont-medium-r-normal-*-14-*-*-*-*-*-iso8859-*")
;; (set-font "-*-unifont-medium-r-normal-*-14-*-*-*-*-*-iso10646-*")
;; (set-font "-*-comic sans ms-medium-r-normal-*-12-*-*-*-*-*-*-*")
;; Set the 1337 lisp machine font
;; (set-font "-lispm-fixed-medium-r-normal-*-12-*-*-*-*-*-*-*")
;; Message window font
;(set-font "-xos4-terminus-medium-r-normal--14-140-72-72-c-80-iso8859-15")
