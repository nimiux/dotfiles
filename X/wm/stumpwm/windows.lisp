;(in-package :stumpwm)

(set-focus-color   "DarkGreen")
(set-unfocus-color "DarkRed")

;; window gravity
;; :center :bottom :top
(set-normal-gravity :center)
(set-normal-gravity :bottom)

;; Clear rules
;(clear-window-placement-rules)
