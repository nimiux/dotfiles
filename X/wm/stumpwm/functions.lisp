;(in-package :stumpwm)

;;; Helper functions
;; concatenates strings, like the Unix command 'cat'
(defun cat (&rest strings)
  (apply 'concatenate 'string strings))

;; slide message
(defun slidemsg (msg &optional (delay 0.05))
  (dotimes (x (1+ (length msg)))
    (sleep delay)
    (message (subseq msg 0 x))))

;; evaluated when WM starts
(defun start-wm ()
  (run-shell-command "3ddeskd --mirror"))

;; evaluated when WM ends
(defun quit-wm ()
  ;; Time to terminate threads if any
  ;(sb-thread:terminate-thread myt)
  (!exit))

;; create groups
(defun create-groups (groups)
  (mapcar (lambda (gname) (add-group (current-screen)
                                     (string-capitalize gname)
                                     :background t))
          (mapcar #'symbol-name groups)))

;; selects random file
(defun select-random-file (dir)
  "Select a random file from directory"
  (let ((file-list (directory dir))
        (*random-state* (make-random-state t)))
    (namestring (nth (random (length file-list)) file-list))))

;; picks up a random image
(defun pickup-random-image (dir)
  (let ((random-file (select-random-file (cat dir "*"))))
    random-file))
    ;;(select-random-file (cat random-dir "/*"))))

(defun isdir? (path)
  (let ((test (probe-file path)))
    (and test (pathname-name path))))

(defcommand selran () ()
            (let* ((obj (pickup-random-image "/mnt/key/media/pics/")))
              (message "~A ~A" obj (isdir? obj))))

(define-key *top-map* (kbd "C-s") "selran")

;; sets the background image
;;(run-shell-command (concatenate 'string "display -window root "
;;                                (select-random-file *background-image-path*)))
;;(defun set-background-image (group image-path &optional (image-type "png"))
;;  "Set the background image"
;;  (let ((gname (group-name group)))
;;    (run-shell-command (cat "xsetbg -fullscreen "
;;                            (if (string-equal gname *first-group*
;;                              (select-random-file
;;                                (cat image-path "wallpapers/*." image-type))
;;                              (cat image-path (group-name group) "." image-type)))))))

;; volume control
(defun master-volume (control)
  "Raises or lowers master volume"
  (let ((sign (if (eq control 'up) "+" "-")))
    (message (cat "Volume" sign))
    (run-shell-command (cat "amixer -c 0 -- sset Master playback 5dB" sign))))

;; status of the rotation of screen
(defun screen-rotation-status ()
  (run-shell-command "xrandr | grep `xrandr | grep ' connected' | awk '{print $1}'` | awk '{ print $4}' | sed -e 's/(//'" t))

;;; Hooks
;; display the key sequence in progress (by Male)
;(defun key-press-hook (key key-seq cmd)
;  (declare (ignore-key))
;  (unless (eq *top-map* *resize-map*)
;    (let ((*message-window-gravity :bottom-right))
;      (message "Key sequence: ~A" (print-key-seq (reverse key-seq))))
;    (when (stringp cmd)
;      (sleep 0.5))))

;; idea from <https://github.com/sabetts/stumpwm/wiki/FAQ>
(defun key-press-msg (key key-seq cmd)
  "Show a message with current incomplete key sequence."
  (declare (ignore key))
  (or (eq *top-map* *resize-map*)
      (stringp cmd)
      (let ((*message-window-gravity* :top-right))
        (message "~A" (print-key-seq (reverse key-seq))))))

;; PENDING
;;(defun focus-group-hook (curr last)
;;  (set-background-image curr *background-image-path*))

;; EXAMPLE
(defun terminal ()
  (message (group-name (current-group))))

