;(in-package :stumpwm)

(create-groups *groups*)
(run-commands "gkill")
;;(setf (group-name (car (screen-groups (current-screen)))) *first-group*)

;; Last rule to match takes precedence!
;; TIP: if the argument to :title or :role begins with an ellipsis, a substring
;; match is performed.
;; TIP: if the :create flag is set then a missing group will be created and
;; restored from *data-dir*/create file.
;; TIP: if the :restore flag is set then group dump is restored even for an
;; existing group using *data-dir*/restore file.
(define-frame-preference "Default")
  ;; frame raise lock (lock AND raise == jumpto)
;;  (0 t nil :class "Konqueror" :role "...konqueror-mainwindow")
;;  (1 t nil :class "XTerm"))

;;(define-frame-preference "Ardour"
;;  (0 t   t   :instance "ardour_editor" :type :normal)
;;  (0 t   t   :title "Ardour - Session Control")
;;  (0 nil nil :class "XTerm")
;;  (1 t   nil :type :normal)
;;  (1 t   t   :instance "ardour_mixer")
;;  (2 t   t   :instance "jvmetro")
;;  (1 t   t   :instance "qjackctl")
;;  (3 t   t   :instance "qjackctl" :role "qjackctlMainForm"))

;;(define-frame-preference "Shareland"
;;  (0 t   nil :class "XTerm")
;;  (1 nil t   :class "aMule"))
;;
;;(define-frame-preference "Emacs"
;;  (1 t t :restore "emacs-editing-dump" :title "...xdvi")
;;  (0 t t :create "emacs-dump" :class "Emacs"))
;;
;;; ???
;;(defparameter *app-menu* '(("BookShelf"
;;                            ;;submenu
;;                            ("Concrete Mathematics" "xpdf /mnt/e/Books/Math/ConcreteMath_CN.pdf")
;;                            ("PraiseForPracticalCommonLisp" "xpdf /mnt/e/Books/CommonLisp/PraiseForPracticalCommonLisp.pdf")
;;                            ("ANSI Common Lisp" "xpdf '/mnt/e/Books/CommonLisp/Graham, Paul - ANSI Common Lisp.pdf'")
;;                            ("Instructors.Manual" "xpdf '/mnt/e/Books/Algorithms/Introduction To Algorithms 2nd Edition Solutions (Instructors.Manual).pdf'")
;;                            ("SolutionsToITA" "xpdf /mnt/e/Books/Algorithms/SolutionsToITA.pdf")
;;                            ("LispBook" "xpdf /mnt/e/Books/CommonLisp/loveinglisp/LispBook.pdf"))
;;
;;                           ("INTERNET"
;;                            ;; sub menu
;;                            ("Firefox" "firefox")
;;                            ("opera" "opera"))
;;                           ("FUN"
;;                            ;; sub menu
;;                            ("option 2" "xlogo")
;;                            ("Crack attack" "crack-attack")
;;                            ("wesnoth" "wesnoth")
;;                            ("supertux" "supertux")
;;                            ("GnuChess" "xboard"))
;;                           ("WORK"
;;                            ;;submenu
;;                            ("OpenOffice.org" "openoffice"))
;;                           ("GRAPHICS"
;;                            ;;submenu
;;                            ("GIMP" "gimp"))))

;;; Web
;(make-web-jump "google" "firefox http://www.google.fr/search?q=")
;(make-web-jump "imdb" "firefox http://www.imdb.com/find?q=")

;; Commands
;(define-stumpwm-command "gmrun" ()
;  (run-shell-command "gmrun"))

;(define-stumpwm-command "conkeror" ()
;  (run-shell-command "firefox"))

;(define-stumpwm-command "firefox" ()
;  (run-shell-command "firefox -chrome chrome://browser/content"))

;(define-stumpwm-command "terminal" ()
;  (run-shell-command terminal))

;(define-stumpwm-command "xlock" ()
;  (run-shell-command "xlock"))

; Thread example
;(defparameter myt (sb-thread:make-thread #'my-function
;                                         :name "function description"))

;; hack to clear level 4 modifiers after wm starts for windows key to work
;  this should be done calling setkbmap: (run-shell-command "setxkbmap -option 'ctrl:swap_lwin_rctl'")
;;(defparameter xt
;;  (sb-thread:make-thread
;;    (lambda ()
;;      (loop
;;        (sleep 5)
;;        (run-shell-command "xmodmap -e 'clear mod4'")
;;        (sleep 10)))
;;    :name "xmodmap"))


;; SBCL-specific; only runs if SBCL is running
;; Call update-mode-line in such and such a way. Borrowed from Luigi Panzeri.
;#+sbcl (defparameter *mode-line-timer* (sb-ext:make-timer
;                                 #'update-mode-line
;                                 :name "mode-line-updating"
;                                 :thread (car (last (sb-thread:list-all-threads)))))
;; Call a function given by the parameter every 30 seconds. This will increment the stuff in the mode-line.
;#+sbcl (sb-ext:schedule-timer *mode-line-timer* 5 :repeat-interval 60 :absolute-p nil)
;; Command for loading swank. Loading it by default could mean a security risk.
;; Of course, people would have to know it's stumpwm and would have to know lisp and..
;; but yeah, security risk.

;#+sbcl(require 'swank)
;#+sbcl(defcommand swank () ()
;		  (setf *top-level-error-action* :break)
;		  (and (swank:create-server :dont-close t)) (echo-string (current-screen) "Starting Swank"))
;(swank)

;; (define-key *root-map* (kbd "C-s") "swank")

;;; Startup apps
;(run-shell-command "lxpanel")
;(stumpwm::run-commands "gkill Emacs"
;                       "gnewbg Emacs"  ;Remember, only one group exists by default
;(mapcar #'run-shell-command
;        (list "xsetbg /home/dummy/wp/0.png"
;              "xmodmap -e 'keysym Alt_L = Meta_L Alt_L'"
;              "xmodmap -e 'clear mod4'"
;              ))
;(mapcar #'run-shell-command
;        '("xsetbg /home/dummy/wp/0.png"
;          "xmodmap -e 'clear mod4'"))

;(slidemsg "All setup dude!")
