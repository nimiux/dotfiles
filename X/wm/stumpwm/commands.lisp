;(in-package :stumpwm)

(defparameter *ws-number* 1)
;;; Commands

;; Creates a new group
(defcommand new-workspace () ()
  "creates a new workspace"
  (setf *ws-number* (+ *ws-number* 1)))

;;(add-group (current-screen)
;;          (write-to-string *ws-number*)
;;         :background t))

;; prompt the user for an interactive command. The first arg is an optional initial contents.
(defcommand colon1 (&optional (initial "")) (:rest)
  (let ((cmd (read-one-line (current-screen) ": " :initial-input initial)))
    (when cmd (eval-command cmd t))))

;; runs emacs
(defcommand emacs () ()
  "run emacs"
  (run-or-raise "emacs-snapshot -T emacs" '(:title "emacs")))

;; runs an application
(defcommand run-app-as-current-user (command-line) ((:rest "App command line:"))
  "Executes an app"
  (let ((cmd (format nil "sudo su - ~A -c \"~A\""
                     (string-downcase (group-name (current-group)))
                     command-line)))
    (message cmd)
    (run-shell-command cmd)))

;; runs an application throug ssh
(defcommand run-app-remote (machine &rest app) ((:rest "Machine: "))
  "Executes an app terminal in a remote machine"
  (let* ((user (string-downcase (group-name (current-group))))
         (cmd (format nil "sudo ssh ~A@~A ~A"
                      user
                      machine
                      app
                      )))
    (message cmd)
    ;(run-shell-command cmd)
    ))

;; runs a terminal
(defcommand run-terminal (machine) ((:rest "Machine: "))
  "Executes a terminal in a remote machine"
  (let* ((user (string-downcase (group-name (current-group))))
         (cmd (format nil "terminology -e \"sudo ssh ~A@~A\""
                      user
                      machine)))
    (message cmd)
    (run-shell-command cmd)))

;; runs a command in bash
(defcommand run-bash-command (command-line) ((:rest "Command line:"))
  "Executes a command in the users bash environment"
  (let* ((user (string-downcase (group-name (current-group))))
         (cmd (format nil "sudo -u ~A terminology -e \"BASH_ENV=/home/~A/.bash.bashrc /bin/bash -c '~A'\""
                      user
                      user
                      command-line)))
    (message cmd)
    (run-shell-command cmd)))

(defcommand raise-master-volume () ()
  "Increments volume"
  (master-volume 'up))

(defcommand lower-master-volume () ()
  "Decrements volume"
  (master-volume 'down))

(defcommand rotate-screen () ()
  "Toggle screen rotation"
  (message (screen-rotation-status)))

;;(defcommand firefox () ()
;;              "run firefox"
;;                (run-or-raise "firefox" '(:class "Firefox")))

;;(defcommand chromium () ()
;;              "run chromium"
;;                (run-or-raise "chromium-browser" '(:instance "chromium-browser")))

;;(defcommand aumix () ()
;;              "run aumix"
;;                (run-or-raise "xterm -name aumix -e aumix" '(:instance "aumix")))

;;(defcommand xterm-1 () ()
;;              "run an xterm instance"
;;                (run-or-raise "xterm -name xterm1 -e 'cd /data/txt && zsh'" '(:instance "xterm1")))

;;(defcommand xterm-2 () ()
;;              "run an xterm instance"
;;                (run-or-raise "xterm -name xterm2" '(:instance "xterm2")))

