;;;; These functions are not used atm

;; Update the mode-line sooner than usual
(defun update-mode-line ()
  (let ((screen (current-screen)))
    (when (screen-mode-line screen)
      (redraw-mode-line-for (screen-mode-line screen) screen))))

;; current window title
(defun current-window-title ()
  (let ((current-window (current-window)))
    (if current-window
        (window-title current-window)
        (concat "No Window In ::"
             (group-name (current-group)) "::"))))

;; toggle screen rotation
(let ((rotate ""))
  (defun toggle-screen-rotation ()
    (run-shell-command (cat "xrandr --output DIV-0 --rotate "
                            (message (cat "xrandr --output LVDS1 --rotate "
                                          (setf rotate
                                                (or (and (string-equal rotate "left")
                                                         "normal")
                                                    "left"))))))))
;; battery's percentage charge
(defun battery-status ()
  (when (probe-file "/proc/acpi/battery/BAT0/")
    (let ((total-charge
            (run-shell-command "grep 'last full capacity:' /proc/acpi/battery/BAT0/info | awk '{print $4}'" t))
          (current-charge
            (run-shell-command "grep 'remaining capacity:' /proc/acpi/battery/BAT0/state | awk '{print $3}'" t)))
      (format nil " | ~D%"  (round  (* (float (/ (parse-integer current-charge)
                                                  (parse-integer total-charge)))
                                        100))))))
(defcommand getbatterystatus () ()
  "Gives you the battery status in a message"
  (process-command ("/usr/bin/acpitool -b")
    (slidemsg line)))
