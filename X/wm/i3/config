# Should you change your keyboard layout some time, delete
# this file and re-run i3-config-wizard(1).

# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

# Default border style for new windows
default_border pixel 1
default_floating_border pixel

# Mod key
set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
#font pango:monospace 8

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8 

# Start XDG autostart .desktop files using dex. See also
# https://wiki.archlinux.org/index.php/XDG_Autostart
#exec --no-startup-id dex --autostart --environment i3

# The combination of xss-lock, nm-applet and pactl is a popular choice, so
# they are included here as an example. Modify as you see fit.

# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
exec --no-startup-id xss-lock --transfer-sleep-lock -- ${HOME}/bin/lockscreen.sh

# NetworkManager is the most popular way to manage wireless networks on Linux,
# and nm-applet is a desktop environment-independent system tray GUI for it.
#exec --no-startup-id nm-applet

# Screenshots
bindsym Print exec --no-startup-id maim "$IMGPATH/$(date +%Y%m%d-%H%M%S-%a).png"
bindsym $mod+Print exec --no-startup-id maim --window $(xdotool getactivewindow) "$IMGPATH/$(date +%Y%m%d-%H%M%S-%a).png"
bindsym Shift+Print exec --no-startup-id maim --select "$IMGPATH/$(date +%Y%m%d-%H%M%S-%a).png"

## Clipboard Screenshots
bindsym Ctrl+Print exec --no-startup-id maim | xclip -selection clipboard -t image/png
bindsym Ctrl+$mod+Print exec --no-startup-id maim --window $(xdotool getactivewindow) | xclip -selection clipboard -t image/png
bindsym Ctrl+Shift+Print exec --no-startup-id maim --select | xclip -selection clipboard -t image/png

# Use pactl to adjust volume in PulseAudio.
set $refresh_i3status killall -SIGUSR1 i3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status
bindsym XF86TouchpadToggle exec ${HOME}/bin/touchpadtoggle.sh

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Program launchers
bindsym $mod+space exec --no-startup-id dmenu_run -fn 'Fantasque Sans Mono:size=18:style=bold'   -i -nf \#FEAFEA
# There also is i3-dmenu-desktop which only displays applications shipping a .desktop file.
# It is a wrapper around dmenu, so you need that installed.
#bindsym $mod+space exec --no-startup-id i3-dmenu-desktop

# Show windows
#bindsym $mod+Escape exec ${HOME}/bin/menu.sh
# Use rofi combi
#bindsym $mod+Escape exec "rofi -show combi"
bindsym $mod+Escape exec  "notify-send 'TO BE ASSIGNED'"

# start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# function keys
bindsym $mod+F1 exec ${TERMINAL} -e emacsclient -create-frame --alternate-editor='' -nw
bindsym $mod+F2 exec ${BROWSER}
bindsym $mod+F3 exec pcmanfm
bindsym $mod+F4 exec libreoffice
bindsym $mod+F5 exec thunderbird
bindsym $mod+F6 exec gimp
bindsym $mod+F7 exec xsane
bindsym $mod+F8 exec ${BROWSER}
bindsym $mod+F9 exec ${BROWSER}
bindsym $mod+F10 exec ${BROWSER}
bindsym $mod+F11 exec ${BROWSER}
bindsym $mod+F12 exec ${BROWSER}

# focus the parent container
#bindsym $mod+a focus parent
# launch app
bindsym $mod+a exec "rofi -modi drun,run -show drun"

# lock the screen
bindsym $mod+b exec ${HOME}/bin/lockscreen.sh

# focus the child container
bindsym $mod+d focus child

# file manager
bindsym $mod+e exec ${TERMINAL} -e vifm

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# split in horizontal orientation
#bindsym $mod+h split h

# bluetooth
bindsym $mod+o exec ${HOME}/bin/toggle-service bluetooth

# music player daemon
bindsym $mod+p exec ${TERMINAL} -e ncmpc

# suspend
bindsym $mod+q exec systemctl suspend

# tilda
bindsym $mod+t exec --no-startup-id tilda
for_window [class="Tilda"] floating enable

# umount media
bindsym $mod+u exec udiskie-umount -a

# split in vertical orientation
bindsym $mod+v split v

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
#bindsym $mod+e layout toggle split

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# kill focused window
bindsym $mod+Shift+k kill

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
#bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# toggle tiling / floating
bindsym $mod+Shift+Space floating toggle

# Workspaces
# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1:editor"
set $ws2 "2:browser"
set $ws3 "3:terminal"
set $ws4 "4:filemanager"
set $ws5 "5:other"
set $ws6 "6:mail"
set $ws7 "7:extra"
#set $ws8 "8"
#set $ws9 "9"
#set $ws9 "10"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
#bindsym $mod+8 workspace number $ws8
#bindsym $mod+9 workspace number $ws9
#bindsym $mod+0 workspace number $ws10

# Navigate through workspaces
bindsym $mod+Tab workspace next
bindsym $mod+Shift+Tab workspace prev

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
#bindsym $mod+Shift+8 move container to workspace number $ws8
#bindsym $mod+Shift+9 move container to workspace number $ws9
#bindsym $mod+Shift+0 move container to workspace number $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+q exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
    # These bindings trigger as soon as you enter the resize mode

    # Pressing left will shrink the window’s width.
    # Pressing right will grow the window’s width.
    # Pressing up will shrink the window’s height.
    # Pressing down will grow the window’s height.
    bindsym j resize shrink width 10 px or 10 ppt
    bindsym k resize grow height 10 px or 10 ppt
    bindsym l resize shrink height 10 px or 10 ppt
    bindsym ntilde resize grow width 10 px or 10 ppt

    # same bindings, but for the arrow keys
    bindsym Left resize shrink width 10 px or 10 ppt
    bindsym Down resize grow height 10 px or 10 ppt
    bindsym Up resize shrink height 10 px or 10 ppt
    bindsym Right resize grow width 10 px or 10 ppt

    # back to normal: Enter or Escape or $mod+r
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
#bar {
#    status_command i3status
#    position bottom
#}
exec polybar -r

# Wallpaper
exec nitrogen --restore

# Lock
exec pkill xautolock
#exec xautolock -time 2 -locker ${HOME}/bin/lockscreen.sh &
#exec xautolock -time 1 -locker slock &
#exec xautolock -time 5 -locker "slock -l" -notify 30 -notifier "notify-send 'Locker' 'Locking screen in 30 seconds'" -killtime 5 -killer "systemctl suspend" &

#setxkbmap -layout es -option ctrl:nocaps 
#setxkbmap -option ctrl:swapcaps es
#exec "setxkbmap -layout es,us ctrl:nocaps"
#exec "setxkbmap -option 'grp:alt_shift_toggle'"
#exec_always "setxkbmap -model pc104 -layout es,us -variant ,, -option grp:alt_shift_toggle -option ctrl:nocaps"
exec_always "setxkbmap -model pc104 -layout es,us -option grp:alt_shift_toggle -option ctrl:nocaps"

exec --no-startup-ed emacs -nw
#exec ${TERMINAL} -e emacs -nw
exec --no-startup-id i3-msg "workspace 2:browser; exec ${BROWSER}"
#exec --no-startup-id i3-msg "workspace 3:terminal; exec ${TERMINAL}"
exec --no-startup-id i3-msg "workspace 7:mail; exec thunderbird"

